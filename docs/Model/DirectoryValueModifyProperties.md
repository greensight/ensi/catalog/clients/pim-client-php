# # DirectoryValueModifyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**preload_file_id** | **int** | Идентификатор предзагруженного ранее файла | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


