# # BindCategoryPropertiesRequestProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор атрибута товара | 
**is_required** | **bool** | Является обязательным для заполнения внутри категории | 
**is_gluing** | **bool** | Свойство используется для склейки товаров | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


