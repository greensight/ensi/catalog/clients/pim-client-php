# # Brand

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор бренда | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**logo_file** | [**\Ensi\PimClient\Dto\File**](File.md) |  | [optional] 
**name** | **string** | Название бренда | [optional] 
**is_active** | **bool** | Активность бренда | [optional] 
**code** | **string** | Код бренда | [optional] 
**description** | **string** | Описание бренда | [optional] 
**logo_url** | **string** | URL логотипа на внешнем сервере | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


