# # SearchProductFieldsFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор(ы) полей | [optional] 
**name** | **string** | Наименование(я) поля | [optional] 
**is_required** | **bool** | Признак обязательности заполнения | [optional] 
**is_moderated** | **bool** | Признак модерируемого поля | [optional] 
**metrics_category** | **string** | Категория метрики товара из перечисления MetricsCategoryEnum | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


