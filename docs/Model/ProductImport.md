# # ProductImport

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**created_at** | [**\DateTime**](\DateTime.md) | Create time | 
**updated_at** | [**\DateTime**](\DateTime.md) | Update time | 
**status** | **int** | Import status from ProductImportStatusEnum | 
**file** | [**\Ensi\PimClient\Dto\File**](File.md) |  | 
**chunks_count** | **int** | Count of scheduled queue jobs | 
**chunks_finished** | **int** | Count of finished queue jobs | 
**type** | **int** | Import type from ProductImportTypeEnum | [optional] 
**warnings** | [**\Ensi\PimClient\Dto\ProductImportWarning[]**](ProductImportWarning.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


