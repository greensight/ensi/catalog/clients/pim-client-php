# # ErrorResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**object**](.md) |  | 
**errors** | [**\Ensi\PimClient\Dto\Error[]**](Error.md) | Массив ошибок | 
**meta** | [**object**](.md) | Объект с мета-информацией | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


