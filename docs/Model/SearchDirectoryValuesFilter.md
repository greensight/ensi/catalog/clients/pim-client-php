# # SearchDirectoryValuesFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор(ы) значений | [optional] 
**name** | **string** | Название или часть названия значения | [optional] 
**code** | **string** | Код(ы) значений | [optional] 
**property_id** | **int** | Идентификатор(ы) атрибута, которому принадлежит значение | [optional] 
**value** | **string** | Значение элемента справочника (string|number|integer|boolean) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


