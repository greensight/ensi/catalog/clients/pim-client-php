# # SearchFailedJobsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\PimClient\Dto\FailedJob[]**](FailedJob.md) |  | 
**meta** | [**\Ensi\PimClient\Dto\SearchProductFieldsResponseMeta**](SearchProductFieldsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


