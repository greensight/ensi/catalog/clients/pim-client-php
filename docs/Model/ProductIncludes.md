# # ProductIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand** | [**\Ensi\PimClient\Dto\Brand**](Brand.md) |  | [optional] 
**categories** | [**\Ensi\PimClient\Dto\Category[]**](Category.md) |  | [optional] 
**category_product_links** | [**\Ensi\PimClient\Dto\CategoryProductLink[]**](CategoryProductLink.md) |  | [optional] 
**images** | [**\Ensi\PimClient\Dto\ProductImage[]**](ProductImage.md) |  | [optional] 
**attributes** | [**\Ensi\PimClient\Dto\ProductAttributeValue[]**](ProductAttributeValue.md) |  | [optional] 
**product_groups** | [**\Ensi\PimClient\Dto\ProductGroup[]**](ProductGroup.md) |  | [optional] 
**no_filled_required_attributes** | **bool** | Product has not filled required attributes | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


