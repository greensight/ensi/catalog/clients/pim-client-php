# # SearchProductFlagsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sort** | **string[]** |  | [optional] 
**pagination** | [**\Ensi\PimClient\Dto\RequestBodyPagination**](RequestBodyPagination.md) |  | [optional] 
**filter** | [**\Ensi\PimClient\Dto\SearchProductFlagsFilter**](SearchProductFlagsFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


