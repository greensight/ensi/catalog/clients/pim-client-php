# # PropertyIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**directory** | [**\Ensi\PimClient\Dto\AttributeDirectoryValue[]**](AttributeDirectoryValue.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


