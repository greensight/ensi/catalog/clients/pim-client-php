# # CreateProductGroupRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category_id** | **int** |  | [optional] 
**main_product_id** | **int** | ID of a product to be used as a cover | [optional] 
**name** | **string** |  | [optional] 
**is_active** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


