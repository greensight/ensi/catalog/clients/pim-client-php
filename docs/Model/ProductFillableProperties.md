# # ProductFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_id** | **string** |  | [optional] 
**barcode** | **string** | EAN | [optional] 
**code** | **string** | Code to use in URL | [optional] 
**vendor_code** | **string** | Article | [optional] 
**category_ids** | **int[]** |  | [optional] 
**brand_id** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**type** | **int** | Product type from ProductTypeEnum | [optional] 
**allow_publish** | **bool** |  | [optional] 
**weight** | **float** | Net weight in kg | [optional] 
**weight_gross** | **float** | Gross weight in kg | [optional] 
**length** | **float** | Length in mm | [optional] 
**width** | **float** | Width in mm | [optional] 
**height** | **float** | Height in mm | [optional] 
**is_adult** | **bool** | Is product 18+ | [optional] 
**uom** | **int** | Unit of measurement from ProductUomEnum | [optional] 
**order_step** | **float** |  | [optional] 
**order_minvol** | **float** | Minimum quantity for order | [optional] 
**picking_weight_deviation** | **float** | Weight deviation limit in percent | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


