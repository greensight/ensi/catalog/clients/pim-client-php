# # StatusSettingIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_statuses** | [**\Ensi\PimClient\Dto\StatusSettingProperties[]**](StatusSettingProperties.md) |  | [optional] 
**previous_statuses** | [**\Ensi\PimClient\Dto\StatusSettingProperties[]**](StatusSettingProperties.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


