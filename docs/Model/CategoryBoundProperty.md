# # CategoryBoundProperty

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор связи | [optional] 
**property_id** | **int** | Идентификатор свойства | [optional] 
**name** | **string** | Название свойства | [optional] 
**display_name** | **string** | Публичное наименование | [optional] 
**code** | **string** | Код свойства | [optional] 
**type** | **string** | Тип свойства из PropertyTypeEnum | [optional] 
**hint_value** | **string** | Подсказка при заполнении для значения | [optional] 
**hint_value_name** | **string** | Подсказка при заполнении для названия значения | [optional] 
**is_multiple** | **bool** | Поддерживает множественные значения | [optional] 
**is_filterable** | **bool** | Выводится в фильтрах на витрине | [optional] 
**is_public** | **bool** | Выводить на витрине | [optional] 
**is_active** | **bool** | Активность атрибут | [optional] 
**has_directory** | **bool** | Атрибут имеет справочник значений | [optional] 
**is_inherited** | **bool** | Наследуемой от родительской категории атрибут | [optional] 
**is_required** | **bool** | Атрибут обязателен для заполнения | [optional] 
**is_common** | **bool** | Признак общего для всех категорий атрибута | [optional] 
**is_system** | **bool** | Признак системного атрибута | [optional] 
**is_moderated** | **bool** | Атрибут модерируется | [optional] 
**is_gluing** | **bool** | Свойство используется для склейки товаров | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


