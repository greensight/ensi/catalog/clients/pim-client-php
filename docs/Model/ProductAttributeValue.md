# # ProductAttributeValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**type** | **string** | Value type from PropertyTypeEnum | 
**file** | [**\Ensi\PimClient\Dto\File**](File.md) |  | [optional] 
**property_id** | **int** |  | [optional] 
**value** | **string** | Value from a dictionary (string|number|integer|boolean) | [optional] 
**name** | **string** | Value name (ex. color name) | [optional] 
**directory_value_id** | **int** | ID of value from dictionary. If filled, value and name fields are ignored | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


