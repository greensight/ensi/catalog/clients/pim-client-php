# # AttributeDirectoryValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор значения | 
**type** | **string** | Тип значения из перечисления PropertyTypeEnum | 
**file** | [**\Ensi\PimClient\Dto\FileNullable**](FileNullable.md) |  | [optional] 
**value** | **string** | Значение элемента справочника (string|number|integer|boolean) Обязателен для заполнения, если не задан preload_file_id. | [optional] 
**name** | **string** | Название значения (например, имя цвета) | [optional] 
**code** | **string** | Символьный код значения | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


