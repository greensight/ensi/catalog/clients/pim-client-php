# # ProductStatusProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_id** | **int** | Status ID from ProductStatusSetting object | [optional] 
**status_comment** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


