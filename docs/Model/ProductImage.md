# # ProductImage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**file** | [**\Ensi\PimClient\Dto\File**](File.md) |  | [optional] 
**name** | **string** | Short image description | [optional] 
**sort** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


