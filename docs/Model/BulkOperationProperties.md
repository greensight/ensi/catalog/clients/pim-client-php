# # BulkOperationProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор товара | 
**created_by** | **string** | Пользователь, инициировавший операцию | [optional] 
**action** | [**\Ensi\PimClient\Dto\BulkOperationActionEnum**](BulkOperationActionEnum.md) |  | 
**entity** | [**\Ensi\PimClient\Dto\BulkOperationEntityEnum**](BulkOperationEntityEnum.md) |  | 
**status** | [**\Ensi\PimClient\Dto\BulkOperationStatusEnum**](BulkOperationStatusEnum.md) |  | 
**ids** | **int[]** | Идентификаторы для обработки | 
**error_message** | **string** | Сообщение об ошибке | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


