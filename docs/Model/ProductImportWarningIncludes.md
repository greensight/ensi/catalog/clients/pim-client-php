# # ProductImportWarningIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**import** | [**\Ensi\PimClient\Dto\ProductImport**](ProductImport.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


