# # File

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **string** | Путь до файла относительно корня диска домена | [optional] 
**root_path** | **string** | Путь до файла относительно корня физического диска ensi | [optional] 
**url** | **string** | Ссылка для скачивания файла | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


