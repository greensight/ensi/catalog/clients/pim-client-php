# # ProductEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**event_id** | **int** | Event ID from ProductEventEnum | 
**product_id** | **int** | Product ID | 
**created_at** | [**\DateTime**](\DateTime.md) | Created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Updated at | 
**product** | [**\Ensi\PimClient\Dto\ProductDraft**](ProductDraft.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


