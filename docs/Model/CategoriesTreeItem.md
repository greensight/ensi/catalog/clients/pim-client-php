# # CategoriesTreeItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор категории | 
**name** | **string** | Название категории | 
**code** | **string** | Код категории | 
**children** | **object[]** | Дочерние категории, если есть | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


