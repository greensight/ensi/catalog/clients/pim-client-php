# # ProductAttributeValueEditProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**preload_file_id** | **int** | ID of the preloaded file. Only valid for attributes of type IMAGE. If set, the value and directory_value_id fields must be absent. The name field can be set, for example, to describe the image. | [optional] 
**mark_to_delete** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


