# # ProductDraft

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**tariffing_volume** | **int** | Unit of tariffication from ProductTariffingVolumeEnum | 
**created_at** | [**\DateTime**](\DateTime.md) | Create time | 
**updated_at** | [**\DateTime**](\DateTime.md) | Update time | 
**external_id** | **string** |  | [optional] 
**barcode** | **string** | EAN | [optional] 
**code** | **string** | Code to use in URL | [optional] 
**vendor_code** | **string** | Article | [optional] 
**category_ids** | **int[]** |  | [optional] 
**brand_id** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**type** | **int** | Product type from ProductTypeEnum | [optional] 
**allow_publish** | **bool** |  | [optional] 
**weight** | **float** | Net weight in kg | [optional] 
**weight_gross** | **float** | Gross weight in kg | [optional] 
**length** | **float** | Length in mm | [optional] 
**width** | **float** | Width in mm | [optional] 
**height** | **float** | Height in mm | [optional] 
**is_adult** | **bool** | Is product 18+ | [optional] 
**uom** | **int** | Unit of measurement from ProductUomEnum | [optional] 
**order_step** | **float** |  | [optional] 
**order_minvol** | **float** | Minimum quantity for order | [optional] 
**picking_weight_deviation** | **float** | Weight deviation limit in percent | [optional] 
**status_id** | **int** | Status ID from ProductStatusSetting object | [optional] 
**status_comment** | **string** |  | [optional] 
**brand** | [**\Ensi\PimClient\Dto\Brand**](Brand.md) |  | [optional] 
**categories** | [**\Ensi\PimClient\Dto\Category[]**](Category.md) |  | [optional] 
**category_product_links** | [**\Ensi\PimClient\Dto\CategoryProductLink[]**](CategoryProductLink.md) |  | [optional] 
**images** | [**\Ensi\PimClient\Dto\ProductImage[]**](ProductImage.md) |  | [optional] 
**attributes** | [**\Ensi\PimClient\Dto\ProductAttributeValue[]**](ProductAttributeValue.md) |  | [optional] 
**product_groups** | [**\Ensi\PimClient\Dto\ProductGroup[]**](ProductGroup.md) |  | [optional] 
**no_filled_required_attributes** | **bool** | Product has not filled required attributes | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


