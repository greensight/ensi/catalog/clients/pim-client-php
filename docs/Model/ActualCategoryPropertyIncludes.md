# # ActualCategoryPropertyIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | [**\Ensi\PimClient\Dto\Category**](Category.md) |  | [optional] 
**property** | [**\Ensi\PimClient\Dto\Property**](Property.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


