# # BulkOperationError

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operation_id** | **int** | Идентификатор массовой операции | 
**entity_id** | **int** | Идентификатор сущности | 
**message** | **string** | Ошибка | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


