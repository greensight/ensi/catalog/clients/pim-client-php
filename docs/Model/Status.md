# # Status

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Cтатус продукта | 
**name** | **string** | Название статуса | 
**available** | **bool** | Доступность из текущего статуса | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


