# # CategoryIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**properties** | [**\Ensi\PimClient\Dto\CategoryBoundProperty[]**](CategoryBoundProperty.md) |  | [optional] 
**hidden_properties** | [**\Ensi\PimClient\Dto\CategoryBoundProperty[]**](CategoryBoundProperty.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


