# # BindProductGroupProductsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**replace** | **bool** | If true, all gluing links not specified in the request will be deleted | [optional] [default to false]
**products** | [**\Ensi\PimClient\Dto\BindProductGroupProductsRequestProducts[]**](BindProductGroupProductsRequestProducts.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


