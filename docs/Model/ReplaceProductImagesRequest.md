# # ReplaceProductImagesRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**images** | [**\Ensi\PimClient\Dto\EditProductImage[]**](EditProductImage.md) | Replace all existing images | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


