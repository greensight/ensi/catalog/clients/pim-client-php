# # DirectoryValueReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор значения | 
**type** | **string** | Тип значения из перечисления PropertyTypeEnum | 
**file** | [**\Ensi\PimClient\Dto\FileNullable**](FileNullable.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


