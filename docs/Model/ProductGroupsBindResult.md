# # ProductGroupsBindResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_group** | [**\Ensi\PimClient\Dto\ProductGroup**](ProductGroup.md) |  | 
**product_errors** | **int[]** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


