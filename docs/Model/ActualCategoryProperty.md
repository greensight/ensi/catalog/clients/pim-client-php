# # ActualCategoryProperty

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**category_id** | **int** | Category ID | 
**property_id** | **int** | Property ID | 
**created_at** | [**\DateTime**](\DateTime.md) | Date of creation | 
**updated_at** | [**\DateTime**](\DateTime.md) | Date of update | 
**is_required** | **bool** | The attribute must be filled in | [optional] 
**is_gluing** | **bool** | The property is used for gluing products | [optional] 
**is_inherited** | **bool** | The property is inherited from parent category | [optional] 
**is_common** | **bool** | The property is common to all categories | [optional] 
**category** | [**\Ensi\PimClient\Dto\Category**](Category.md) |  | [optional] 
**property** | [**\Ensi\PimClient\Dto\Property**](Property.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


