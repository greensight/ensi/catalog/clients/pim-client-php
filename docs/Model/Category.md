# # Category

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор категории | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**is_real_active** | **bool** | Признак активности с учетом иерархии | 
**name** | **string** | Название категории | [optional] 
**code** | **string** | Код категории | [optional] 
**parent_id** | **int** | Идентификатор родительской категории | [optional] 
**is_inherits_properties** | **bool** | Категория наследует атрибуты родительской | [optional] 
**is_active** | **bool** | Признак активности данной категории | [optional] 
**properties** | [**\Ensi\PimClient\Dto\CategoryBoundProperty[]**](CategoryBoundProperty.md) |  | [optional] 
**hidden_properties** | [**\Ensi\PimClient\Dto\CategoryBoundProperty[]**](CategoryBoundProperty.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


