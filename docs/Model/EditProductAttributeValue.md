# # EditProductAttributeValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**property_id** | **int** |  | [optional] 
**value** | **string** | Value from a dictionary (string|number|integer|boolean) | [optional] 
**name** | **string** | Value name (ex. color name) | [optional] 
**directory_value_id** | **int** | ID of value from dictionary. If filled, value and name fields are ignored | [optional] 
**preload_file_id** | **int** | ID of the preloaded file. Only valid for attributes of type IMAGE. If set, the value and directory_value_id fields must be absent. The name field can be set, for example, to describe the image. | [optional] 
**mark_to_delete** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


