# # MassOperationResultData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**processed** | **int[]** |  | 
**errors** | [**\Ensi\PimClient\Dto\MassOperationResultDataErrors[]**](MassOperationResultDataErrors.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


