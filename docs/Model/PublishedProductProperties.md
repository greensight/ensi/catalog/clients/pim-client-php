# # PublishedProductProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**main_image_file** | [**\Ensi\PimClient\Dto\FileNullable**](FileNullable.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


