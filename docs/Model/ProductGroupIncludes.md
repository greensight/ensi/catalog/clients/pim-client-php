# # ProductGroupIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | [**\Ensi\PimClient\Dto\Category**](Category.md) |  | [optional] 
**main_product** | [**\Ensi\PimClient\Dto\ProductDraft**](ProductDraft.md) |  | [optional] 
**products** | [**\Ensi\PimClient\Dto\ProductDraft[]**](ProductDraft.md) |  | [optional] 
**product_links** | [**\Ensi\PimClient\Dto\ProductGroupProduct[]**](ProductGroupProduct.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


