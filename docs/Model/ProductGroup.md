# # ProductGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**products_count** | **int** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Create time | 
**updated_at** | [**\DateTime**](\DateTime.md) | Update time | 
**category_id** | **int** |  | [optional] 
**main_product_id** | **int** | ID of a product to be used as a cover | [optional] 
**name** | **string** |  | [optional] 
**is_active** | **bool** |  | [optional] 
**category** | [**\Ensi\PimClient\Dto\Category**](Category.md) |  | [optional] 
**main_product** | [**\Ensi\PimClient\Dto\ProductDraft**](ProductDraft.md) |  | [optional] 
**products** | [**\Ensi\PimClient\Dto\ProductDraft[]**](ProductDraft.md) |  | [optional] 
**product_links** | [**\Ensi\PimClient\Dto\ProductGroupProduct[]**](ProductGroupProduct.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


