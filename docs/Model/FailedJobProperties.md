# # FailedJobProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**uuid** | **string** | Job uuid | 
**connection** | **string** |  | 
**queue** | **string** |  | 
**payload** | **string** | Full message payload | 
**exception** | **string** | Exception (with stack trace) | 
**failed_at** | [**\DateTime**](\DateTime.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


