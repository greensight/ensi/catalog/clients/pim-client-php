# # StatusSettingProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор статуса | 
**created_at** | [**\DateTime**](\DateTime.md) | Время создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления | 
**code** | **string** | Код статуса | [optional] 
**name** | **string** | Название статуса | [optional] 
**color** | **string** | Цвет статуса | [optional] 
**type** | **int** | Тип статуса из ProductStatusTypeEnum | [optional] 
**is_active** | **bool** | Признак активности статуса | [optional] 
**is_publication** | **bool** | Флаг по которому у товара проставляется признак активности для витрины | [optional] 
**events** | [**\Ensi\PimClient\Dto\EventConditions**](EventConditions.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


