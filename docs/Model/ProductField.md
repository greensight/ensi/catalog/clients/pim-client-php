# # ProductField

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор поля | 
**code** | **string** | Символьный код поля | 
**edit_mask** | **int** | Биты из FieldSettingsMaskEnum, соответствующие доступным для редактирования свойствам | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**name** | **string** | Наименование поля товара | [optional] 
**is_required** | **bool** | Признак обязательности заполнения | [optional] 
**is_moderated** | **bool** | Признак модерируемого поля | [optional] 
**metrics_category** | **string** | Категория метрики товара - значение перечисления MetricsCategoryEnum или null | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


