# # ProductReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**tariffing_volume** | **int** | Unit of tariffication from ProductTariffingVolumeEnum | 
**created_at** | [**\DateTime**](\DateTime.md) | Create time | 
**updated_at** | [**\DateTime**](\DateTime.md) | Update time | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


