# # ProductUniqueProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_id** | **string** |  | [optional] 
**barcode** | **string** | EAN | [optional] 
**code** | **string** | Code to use in URL | [optional] 
**vendor_code** | **string** | Article | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


