# # ProductImageCreateProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**preload_file_id** | **int** | ID of preloaded file. If set, new product image object will be created | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


