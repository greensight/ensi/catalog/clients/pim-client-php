# # EventConditions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operation** | **int** | Операция из EventOperationEnum | 
**events** | **int[]** | Список событий | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


