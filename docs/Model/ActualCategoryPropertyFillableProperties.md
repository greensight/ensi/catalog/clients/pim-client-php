# # ActualCategoryPropertyFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_required** | **bool** | The attribute must be filled in | [optional] 
**is_gluing** | **bool** | The property is used for gluing products | [optional] 
**is_inherited** | **bool** | The property is inherited from parent category | [optional] 
**is_common** | **bool** | The property is common to all categories | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


