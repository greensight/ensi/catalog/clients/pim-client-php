# # ProductImportWarningReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**created_at** | [**\DateTime**](\DateTime.md) | Create time | 
**updated_at** | [**\DateTime**](\DateTime.md) | Update time | 
**import_id** | **int** |  | 
**vendor_code** | **string** | SKU of product with error | 
**import_type** | **int** | Import type from ProductImportTypeEnum | 
**message** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


