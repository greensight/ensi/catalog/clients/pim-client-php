# # BrandImageFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**preload_file_id** | **int** | Идентификатор загруженного ранее изображения. Обязателен при создании бренда, если не задан logo_url | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


