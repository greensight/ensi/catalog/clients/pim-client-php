# # MassPatchProductsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ids** | **int[]** | Идентификаторы целевых сущностей | 
**fields** | [**ProductMassPatchableProperties**](ProductMassPatchableProperties.md) |  | [optional] 
**attributes** | [**\Ensi\PimClient\Dto\EditProductAttributeValue[]**](EditProductAttributeValue.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


