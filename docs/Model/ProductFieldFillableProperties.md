# # ProductFieldFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Наименование поля товара | [optional] 
**is_required** | **bool** | Признак обязательности заполнения | [optional] 
**is_moderated** | **bool** | Признак модерируемого поля | [optional] 
**metrics_category** | **string** | Категория метрики товара - значение перечисления MetricsCategoryEnum или null | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


