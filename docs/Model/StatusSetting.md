# # StatusSetting

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор статуса | 
**created_at** | [**\DateTime**](\DateTime.md) | Время создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления | 
**code** | **string** | Код статуса | 
**name** | **string** | Название статуса | 
**color** | **string** | Цвет статуса | 
**type** | **int** | Тип статуса из ProductStatusTypeEnum | 
**is_active** | **bool** | Признак активности статуса | 
**is_publication** | **bool** | Флаг по которому у товара проставляется признак активности для витрины | 
**events** | [**\Ensi\PimClient\Dto\EventConditions**](EventConditions.md) |  | 
**next_statuses** | [**\Ensi\PimClient\Dto\StatusSettingProperties[]**](StatusSettingProperties.md) |  | [optional] 
**previous_statuses** | [**\Ensi\PimClient\Dto\StatusSettingProperties[]**](StatusSettingProperties.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


