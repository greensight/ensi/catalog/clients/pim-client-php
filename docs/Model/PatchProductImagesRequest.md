# # PatchProductImagesRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**images** | [**\Ensi\PimClient\Dto\EditProductImage[]**](EditProductImage.md) | Update only images that are specified in the request | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


