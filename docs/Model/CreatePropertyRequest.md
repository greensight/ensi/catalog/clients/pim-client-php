# # CreatePropertyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Название свойства | [optional] 
**display_name** | **string** | Публичное наименование | [optional] 
**code** | **string** | Код свойства | [optional] 
**type** | **string** | Тип свойства из PropertyTypeEnum | [optional] 
**hint_value** | **string** | Подсказка при заполнении для значения | [optional] 
**hint_value_name** | **string** | Подсказка при заполнении для названия значения | [optional] 
**is_multiple** | **bool** | Поддерживает множественные значения | [optional] 
**is_filterable** | **bool** | Выводится в фильтрах на витрине | [optional] 
**is_public** | **bool** | Выводить на витрине | [optional] 
**is_active** | **bool** | Активность атрибута | [optional] 
**is_required** | **bool** | Обязательность заполнения для общего (is_common&#x3D;true) атрибута | [optional] 
**has_directory** | **bool** | Атрибут имеет справочник значений | [optional] 
**is_moderated** | **bool** | Атрибут модерируется | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


