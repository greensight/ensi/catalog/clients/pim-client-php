# # SearchCategoriesFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор(ы) категории | [optional] 
**name** | **string** | Название категории | [optional] 
**name_like** | **string** | Название или часть названия категории | [optional] 
**code** | **string** | Код(ы) категорий | [optional] 
**code_like** | **string** | Код или часть кода категории | [optional] 
**parent_id** | **int** | Идентификатор(ы) родительской категории | [optional] 
**is_active** | **bool** | Признак активности данной категории | [optional] 
**is_real_active** | **bool** | Признак фактической активности категории с учетом иерархии | [optional] 
**is_root** | **bool** | Отбирать только категории верхнего уровня или наоборот только вложенные | [optional] 
**exclude_id** | **int** | Идентификатор(ы) исключаемой категории | [optional] 
**has_is_gluing** | **bool** | Признак наличия склейки | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


