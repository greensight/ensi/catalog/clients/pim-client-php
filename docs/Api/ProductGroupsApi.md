# Ensi\PimClient\ProductGroupsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bindProductGroupProducts**](ProductGroupsApi.md#bindProductGroupProducts) | **POST** /products/product-groups/{id}:bind-products | Changing product links in a ProductGroup
[**createProductGroup**](ProductGroupsApi.md#createProductGroup) | **POST** /products/product-groups | Creating an object of ProductGroup
[**deleteProductGroup**](ProductGroupsApi.md#deleteProductGroup) | **DELETE** /products/product-groups/{id} | Deleting an object of ProductGroup
[**deleteProductGroups**](ProductGroupsApi.md#deleteProductGroups) | **POST** /products/product-groups:mass-delete | Mass deleting of ProductGroups
[**getProductGroup**](ProductGroupsApi.md#getProductGroup) | **GET** /products/product-groups/{id} | Get the object of ProductGroup by ID
[**patchProductGroup**](ProductGroupsApi.md#patchProductGroup) | **PATCH** /products/product-groups/{id} | Patching an object of ProductGroup
[**replaceProductGroup**](ProductGroupsApi.md#replaceProductGroup) | **PUT** /products/product-groups/{id} | Replacing an object of ProductGroup
[**searchOneProductGroup**](ProductGroupsApi.md#searchOneProductGroup) | **POST** /products/product-groups:search-one | Search for an object of ProductGroup
[**searchProductGroups**](ProductGroupsApi.md#searchProductGroups) | **POST** /products/product-groups:search | Search for objects of ProductGroup



## bindProductGroupProducts

> \Ensi\PimClient\Dto\ProductGroupsBindResponse bindProductGroupProducts($id, $bind_product_group_products_request)

Changing product links in a ProductGroup

Changing product links in a ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$bind_product_group_products_request = new \Ensi\PimClient\Dto\BindProductGroupProductsRequest(); // \Ensi\PimClient\Dto\BindProductGroupProductsRequest | 

try {
    $result = $apiInstance->bindProductGroupProducts($id, $bind_product_group_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->bindProductGroupProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **bind_product_group_products_request** | [**\Ensi\PimClient\Dto\BindProductGroupProductsRequest**](../Model/BindProductGroupProductsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductGroupsBindResponse**](../Model/ProductGroupsBindResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createProductGroup

> \Ensi\PimClient\Dto\ProductGroupResponse createProductGroup($create_product_group_request)

Creating an object of ProductGroup

Creating an object of ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_product_group_request = new \Ensi\PimClient\Dto\CreateProductGroupRequest(); // \Ensi\PimClient\Dto\CreateProductGroupRequest | 

try {
    $result = $apiInstance->createProductGroup($create_product_group_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->createProductGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_product_group_request** | [**\Ensi\PimClient\Dto\CreateProductGroupRequest**](../Model/CreateProductGroupRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductGroupResponse**](../Model/ProductGroupResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductGroup

> \Ensi\PimClient\Dto\EmptyDataResponse deleteProductGroup($id)

Deleting an object of ProductGroup

Deleting an object of ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID

try {
    $result = $apiInstance->deleteProductGroup($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->deleteProductGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductGroups

> \Ensi\PimClient\Dto\MassOperationResult deleteProductGroups($request_body_mass_operation)

Mass deleting of ProductGroups

Mass deleting of ProductGroups

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$request_body_mass_operation = new \Ensi\PimClient\Dto\RequestBodyMassOperation(); // \Ensi\PimClient\Dto\RequestBodyMassOperation | 

try {
    $result = $apiInstance->deleteProductGroups($request_body_mass_operation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->deleteProductGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body_mass_operation** | [**\Ensi\PimClient\Dto\RequestBodyMassOperation**](../Model/RequestBodyMassOperation.md)|  |

### Return type

[**\Ensi\PimClient\Dto\MassOperationResult**](../Model/MassOperationResult.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProductGroup

> \Ensi\PimClient\Dto\ProductGroupResponse getProductGroup($id, $include)

Get the object of ProductGroup by ID

Get the object of ProductGroup by ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getProductGroup($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->getProductGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\PimClient\Dto\ProductGroupResponse**](../Model/ProductGroupResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchProductGroup

> \Ensi\PimClient\Dto\ProductGroupResponse patchProductGroup($id, $patch_product_group_request)

Patching an object of ProductGroup

Patching an object of ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$patch_product_group_request = new \Ensi\PimClient\Dto\PatchProductGroupRequest(); // \Ensi\PimClient\Dto\PatchProductGroupRequest | 

try {
    $result = $apiInstance->patchProductGroup($id, $patch_product_group_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->patchProductGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **patch_product_group_request** | [**\Ensi\PimClient\Dto\PatchProductGroupRequest**](../Model/PatchProductGroupRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductGroupResponse**](../Model/ProductGroupResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProductGroup

> \Ensi\PimClient\Dto\ProductGroupResponse replaceProductGroup($id, $replace_product_group_request)

Replacing an object of ProductGroup

Replacing an object of ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$replace_product_group_request = new \Ensi\PimClient\Dto\ReplaceProductGroupRequest(); // \Ensi\PimClient\Dto\ReplaceProductGroupRequest | 

try {
    $result = $apiInstance->replaceProductGroup($id, $replace_product_group_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->replaceProductGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **replace_product_group_request** | [**\Ensi\PimClient\Dto\ReplaceProductGroupRequest**](../Model/ReplaceProductGroupRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductGroupResponse**](../Model/ProductGroupResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneProductGroup

> \Ensi\PimClient\Dto\ProductGroupResponse searchOneProductGroup($search_product_groups_request)

Search for an object of ProductGroup

Search for an object of ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_groups_request = new \Ensi\PimClient\Dto\SearchProductGroupsRequest(); // \Ensi\PimClient\Dto\SearchProductGroupsRequest | 

try {
    $result = $apiInstance->searchOneProductGroup($search_product_groups_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->searchOneProductGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_groups_request** | [**\Ensi\PimClient\Dto\SearchProductGroupsRequest**](../Model/SearchProductGroupsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductGroupResponse**](../Model/ProductGroupResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductGroups

> \Ensi\PimClient\Dto\SearchProductGroupsResponse searchProductGroups($search_product_groups_request)

Search for objects of ProductGroup

Search for objects of ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_groups_request = new \Ensi\PimClient\Dto\SearchProductGroupsRequest(); // \Ensi\PimClient\Dto\SearchProductGroupsRequest | 

try {
    $result = $apiInstance->searchProductGroups($search_product_groups_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->searchProductGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_groups_request** | [**\Ensi\PimClient\Dto\SearchProductGroupsRequest**](../Model/SearchProductGroupsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchProductGroupsResponse**](../Model/SearchProductGroupsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

