# Ensi\PimClient\ProductStatusesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProductStatus**](ProductStatusesApi.md#createProductStatus) | **POST** /classifiers/product-statuses | Создание объекта типа ProductStatusSetting
[**deleteProductStatus**](ProductStatusesApi.md#deleteProductStatus) | **DELETE** /classifiers/product-statuses/{id} | Удаление объекта типа ProductStatusSetting
[**getProductStatus**](ProductStatusesApi.md#getProductStatus) | **GET** /classifiers/product-statuses/{id} | Получение объекта типа ProductStatusSetting по идентификатору
[**nextProductStatuses**](ProductStatusesApi.md#nextProductStatuses) | **POST** /classifiers/product-statuses:next | Получение доступных статусов из текущего
[**patchProductStatus**](ProductStatusesApi.md#patchProductStatus) | **PATCH** /classifiers/product-statuses/{id} | Изменение объекта типа ProductStatusSetting
[**searchProductStatuses**](ProductStatusesApi.md#searchProductStatuses) | **POST** /classifiers/product-statuses:search | Поиск объектов типа ProductStatusSetting
[**setPreviousProductStatuses**](ProductStatusesApi.md#setPreviousProductStatuses) | **POST** /classifiers/product-statuses/{id}:set-previous | Установка статусов из которых можно перейти в текущий



## createProductStatus

> \Ensi\PimClient\Dto\StatusSettingResponse createProductStatus($create_status_setting_request)

Создание объекта типа ProductStatusSetting

Создание объекта типа ProductStatusSetting

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductStatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_status_setting_request = new \Ensi\PimClient\Dto\CreateStatusSettingRequest(); // \Ensi\PimClient\Dto\CreateStatusSettingRequest | 

try {
    $result = $apiInstance->createProductStatus($create_status_setting_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductStatusesApi->createProductStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_status_setting_request** | [**\Ensi\PimClient\Dto\CreateStatusSettingRequest**](../Model/CreateStatusSettingRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\StatusSettingResponse**](../Model/StatusSettingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductStatus

> \Ensi\PimClient\Dto\EmptyDataResponse deleteProductStatus($id)

Удаление объекта типа ProductStatusSetting

Удаление объекта типа ProductStatusSetting

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductStatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteProductStatus($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductStatusesApi->deleteProductStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProductStatus

> \Ensi\PimClient\Dto\StatusSettingResponse getProductStatus($id, $include)

Получение объекта типа ProductStatusSetting по идентификатору

Получение объекта типа ProductStatusSetting по идентификатору

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductStatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getProductStatus($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductStatusesApi->getProductStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\PimClient\Dto\StatusSettingResponse**](../Model/StatusSettingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## nextProductStatuses

> \Ensi\PimClient\Dto\StatusesResponse nextProductStatuses($get_next_status_request)

Получение доступных статусов из текущего

Получение доступных статусов из текущего

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductStatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$get_next_status_request = new \Ensi\PimClient\Dto\GetNextStatusRequest(); // \Ensi\PimClient\Dto\GetNextStatusRequest | 

try {
    $result = $apiInstance->nextProductStatuses($get_next_status_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductStatusesApi->nextProductStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **get_next_status_request** | [**\Ensi\PimClient\Dto\GetNextStatusRequest**](../Model/GetNextStatusRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\StatusesResponse**](../Model/StatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchProductStatus

> \Ensi\PimClient\Dto\StatusSettingResponse patchProductStatus($id, $patch_status_setting_request)

Изменение объекта типа ProductStatusSetting

Изменение объекта типа ProductStatusSetting

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductStatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_status_setting_request = new \Ensi\PimClient\Dto\PatchStatusSettingRequest(); // \Ensi\PimClient\Dto\PatchStatusSettingRequest | 

try {
    $result = $apiInstance->patchProductStatus($id, $patch_status_setting_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductStatusesApi->patchProductStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_status_setting_request** | [**\Ensi\PimClient\Dto\PatchStatusSettingRequest**](../Model/PatchStatusSettingRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\StatusSettingResponse**](../Model/StatusSettingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductStatuses

> \Ensi\PimClient\Dto\StatusSettingsResponse searchProductStatuses($search_status_settings_request)

Поиск объектов типа ProductStatusSetting

Поиск объектов типа ProductStatusSetting

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductStatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_status_settings_request = new \Ensi\PimClient\Dto\SearchStatusSettingsRequest(); // \Ensi\PimClient\Dto\SearchStatusSettingsRequest | 

try {
    $result = $apiInstance->searchProductStatuses($search_status_settings_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductStatusesApi->searchProductStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_status_settings_request** | [**\Ensi\PimClient\Dto\SearchStatusSettingsRequest**](../Model/SearchStatusSettingsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\StatusSettingsResponse**](../Model/StatusSettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## setPreviousProductStatuses

> \Ensi\PimClient\Dto\StatusSettingResponse setPreviousProductStatuses($id, $set_previous_status_request)

Установка статусов из которых можно перейти в текущий

Установка статусов из которых можно перейти в текущий

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductStatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$set_previous_status_request = new \Ensi\PimClient\Dto\SetPreviousStatusRequest(); // \Ensi\PimClient\Dto\SetPreviousStatusRequest | 

try {
    $result = $apiInstance->setPreviousProductStatuses($id, $set_previous_status_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductStatusesApi->setPreviousProductStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **set_previous_status_request** | [**\Ensi\PimClient\Dto\SetPreviousStatusRequest**](../Model/SetPreviousStatusRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\StatusSettingResponse**](../Model/StatusSettingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

