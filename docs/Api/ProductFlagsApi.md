# Ensi\PimClient\ProductFlagsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProductFlag**](ProductFlagsApi.md#getProductFlag) | **GET** /classifiers/product-flags/{id} | Получение признака по идентификатору
[**replaceProductFlag**](ProductFlagsApi.md#replaceProductFlag) | **PUT** /classifiers/product-flags/{id} | Обновление данных признака
[**searchProductFlags**](ProductFlagsApi.md#searchProductFlags) | **POST** /classifiers/product-flags:search | Поиск признаков товаров, удовлетворяющих условиям отбора



## getProductFlag

> \Ensi\PimClient\Dto\ProductFlagResponse getProductFlag($id)

Получение признака по идентификатору

Получение признака по идентификатору

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductFlagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getProductFlag($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductFlagsApi->getProductFlag: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\ProductFlagResponse**](../Model/ProductFlagResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProductFlag

> \Ensi\PimClient\Dto\ProductFlagResponse replaceProductFlag($id, $replace_product_flag_request)

Обновление данных признака

Обновление данных признака

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductFlagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_product_flag_request = new \Ensi\PimClient\Dto\ReplaceProductFlagRequest(); // \Ensi\PimClient\Dto\ReplaceProductFlagRequest | 

try {
    $result = $apiInstance->replaceProductFlag($id, $replace_product_flag_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductFlagsApi->replaceProductFlag: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_product_flag_request** | [**\Ensi\PimClient\Dto\ReplaceProductFlagRequest**](../Model/ReplaceProductFlagRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductFlagResponse**](../Model/ProductFlagResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductFlags

> \Ensi\PimClient\Dto\SearchProductFlagsResponse searchProductFlags($search_product_flags_request)

Поиск признаков товаров, удовлетворяющих условиям отбора

Поиск признаков товаров, удовлетворяющих условиям отбора

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductFlagsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_flags_request = new \Ensi\PimClient\Dto\SearchProductFlagsRequest(); // \Ensi\PimClient\Dto\SearchProductFlagsRequest | 

try {
    $result = $apiInstance->searchProductFlags($search_product_flags_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductFlagsApi->searchProductFlags: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_flags_request** | [**\Ensi\PimClient\Dto\SearchProductFlagsRequest**](../Model/SearchProductFlagsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchProductFlagsResponse**](../Model/SearchProductFlagsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

