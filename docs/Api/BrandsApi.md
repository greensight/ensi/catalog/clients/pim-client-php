# Ensi\PimClient\BrandsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBrand**](BrandsApi.md#createBrand) | **POST** /classifiers/brands | Запрос на создание нового бренда
[**deleteBrand**](BrandsApi.md#deleteBrand) | **DELETE** /classifiers/brands/{id} | Запрос на удаление бренда по ID
[**deleteBrandImage**](BrandsApi.md#deleteBrandImage) | **POST** /classifiers/brands/{id}:delete-image | Удаление текущей картинки бренда
[**getBrand**](BrandsApi.md#getBrand) | **GET** /classifiers/brands/{id} | Запрос бренда по ID
[**massDeleteBrands**](BrandsApi.md#massDeleteBrands) | **POST** /classifiers/brands:mass-delete | Массовое удаление брендов
[**patchBrand**](BrandsApi.md#patchBrand) | **PATCH** /classifiers/brands/{id} | Обновляет отдельные поля бренда
[**preloadBrandImage**](BrandsApi.md#preloadBrandImage) | **POST** /classifiers/brands:preload-image | Загрузка картинки для бренда
[**replaceBrand**](BrandsApi.md#replaceBrand) | **PUT** /classifiers/brands/{id} | Запрос на обновление бренда
[**searchBrands**](BrandsApi.md#searchBrands) | **POST** /classifiers/brands:search | Поиск брендов, удовлетворяющих фильтру
[**uploadBrandImage**](BrandsApi.md#uploadBrandImage) | **POST** /classifiers/brands/{id}:upload-image | Загрузка картинки для бренда



## createBrand

> \Ensi\PimClient\Dto\BrandResponse createBrand($create_brand_request)

Запрос на создание нового бренда

Запрос на создание нового бренда

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\BrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_brand_request = new \Ensi\PimClient\Dto\CreateBrandRequest(); // \Ensi\PimClient\Dto\CreateBrandRequest | 

try {
    $result = $apiInstance->createBrand($create_brand_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BrandsApi->createBrand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_brand_request** | [**\Ensi\PimClient\Dto\CreateBrandRequest**](../Model/CreateBrandRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\BrandResponse**](../Model/BrandResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteBrand

> \Ensi\PimClient\Dto\EmptyDataResponse deleteBrand($id)

Запрос на удаление бренда по ID

Запрос на удаление бренда по ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\BrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteBrand($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BrandsApi->deleteBrand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteBrandImage

> \Ensi\PimClient\Dto\BrandResponse deleteBrandImage($id)

Удаление текущей картинки бренда

Удаление текущей картинки бренда

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\BrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteBrandImage($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BrandsApi->deleteBrandImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\BrandResponse**](../Model/BrandResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getBrand

> \Ensi\PimClient\Dto\BrandResponse getBrand($id)

Запрос бренда по ID

Запрос бренда по ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\BrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getBrand($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BrandsApi->getBrand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\BrandResponse**](../Model/BrandResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## massDeleteBrands

> \Ensi\PimClient\Dto\EmptyDataResponse massDeleteBrands($request_body_mass_operation)

Массовое удаление брендов

Массовое удаление брендов

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\BrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$request_body_mass_operation = new \Ensi\PimClient\Dto\RequestBodyMassOperation(); // \Ensi\PimClient\Dto\RequestBodyMassOperation | 

try {
    $result = $apiInstance->massDeleteBrands($request_body_mass_operation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BrandsApi->massDeleteBrands: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body_mass_operation** | [**\Ensi\PimClient\Dto\RequestBodyMassOperation**](../Model/RequestBodyMassOperation.md)|  |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchBrand

> \Ensi\PimClient\Dto\BrandResponse patchBrand($id, $patch_brand_request)

Обновляет отдельные поля бренда

Обновляет отдельные поля бренда

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\BrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_brand_request = new \Ensi\PimClient\Dto\PatchBrandRequest(); // \Ensi\PimClient\Dto\PatchBrandRequest | 

try {
    $result = $apiInstance->patchBrand($id, $patch_brand_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BrandsApi->patchBrand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_brand_request** | [**\Ensi\PimClient\Dto\PatchBrandRequest**](../Model/PatchBrandRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\BrandResponse**](../Model/BrandResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## preloadBrandImage

> \Ensi\PimClient\Dto\PreloadFile preloadBrandImage($file)

Загрузка картинки для бренда

Загрузка картинки для бренда

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\BrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->preloadBrandImage($file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BrandsApi->preloadBrandImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\PimClient\Dto\PreloadFile**](../Model/PreloadFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceBrand

> \Ensi\PimClient\Dto\BrandResponse replaceBrand($id, $replace_brand_request)

Запрос на обновление бренда

Запрос на обновление бренда

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\BrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_brand_request = new \Ensi\PimClient\Dto\ReplaceBrandRequest(); // \Ensi\PimClient\Dto\ReplaceBrandRequest | 

try {
    $result = $apiInstance->replaceBrand($id, $replace_brand_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BrandsApi->replaceBrand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_brand_request** | [**\Ensi\PimClient\Dto\ReplaceBrandRequest**](../Model/ReplaceBrandRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\BrandResponse**](../Model/BrandResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchBrands

> \Ensi\PimClient\Dto\SearchBrandsResponse searchBrands($search_brands_request)

Поиск брендов, удовлетворяющих фильтру

Поиск брендов, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\BrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_brands_request = new \Ensi\PimClient\Dto\SearchBrandsRequest(); // \Ensi\PimClient\Dto\SearchBrandsRequest | 

try {
    $result = $apiInstance->searchBrands($search_brands_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BrandsApi->searchBrands: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_brands_request** | [**\Ensi\PimClient\Dto\SearchBrandsRequest**](../Model/SearchBrandsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchBrandsResponse**](../Model/SearchBrandsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## uploadBrandImage

> \Ensi\PimClient\Dto\BrandResponse uploadBrandImage($id, $file)

Загрузка картинки для бренда

Загрузка картинки для бренда

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\BrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->uploadBrandImage($id, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BrandsApi->uploadBrandImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\PimClient\Dto\BrandResponse**](../Model/BrandResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

