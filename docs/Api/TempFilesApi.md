# Ensi\PimClient\TempFilesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchTempFiles**](TempFilesApi.md#searchTempFiles) | **POST** /support/temp-files:search | Search for objects of TempFile



## searchTempFiles

> \Ensi\PimClient\Dto\SearchTempFilesResponse searchTempFiles($search_temp_files_request)

Search for objects of TempFile

Search for objects of TempFile

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\TempFilesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_temp_files_request = new \Ensi\PimClient\Dto\SearchTempFilesRequest(); // \Ensi\PimClient\Dto\SearchTempFilesRequest | 

try {
    $result = $apiInstance->searchTempFiles($search_temp_files_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TempFilesApi->searchTempFiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_temp_files_request** | [**\Ensi\PimClient\Dto\SearchTempFilesRequest**](../Model/SearchTempFilesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchTempFilesResponse**](../Model/SearchTempFilesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

