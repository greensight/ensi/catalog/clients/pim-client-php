# Ensi\PimClient\CommonApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchBulkOperations**](CommonApi.md#searchBulkOperations) | **POST** /common/bulk-operations:search | Search for objects of BulkOperation
[**searchFailedJobs**](CommonApi.md#searchFailedJobs) | **POST** /common/failed-jobs:search | Search for objects of FailedJob



## searchBulkOperations

> \Ensi\PimClient\Dto\SearchBulkOperationsResponse searchBulkOperations($search_bulk_operations_request)

Search for objects of BulkOperation

Search for objects of BulkOperation

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_bulk_operations_request = new \Ensi\PimClient\Dto\SearchBulkOperationsRequest(); // \Ensi\PimClient\Dto\SearchBulkOperationsRequest | 

try {
    $result = $apiInstance->searchBulkOperations($search_bulk_operations_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->searchBulkOperations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_bulk_operations_request** | [**\Ensi\PimClient\Dto\SearchBulkOperationsRequest**](../Model/SearchBulkOperationsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchBulkOperationsResponse**](../Model/SearchBulkOperationsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchFailedJobs

> \Ensi\PimClient\Dto\SearchFailedJobsResponse searchFailedJobs($search_failed_jobs_request)

Search for objects of FailedJob

Search for objects of FailedJob

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_failed_jobs_request = new \Ensi\PimClient\Dto\SearchFailedJobsRequest(); // \Ensi\PimClient\Dto\SearchFailedJobsRequest | 

try {
    $result = $apiInstance->searchFailedJobs($search_failed_jobs_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->searchFailedJobs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_failed_jobs_request** | [**\Ensi\PimClient\Dto\SearchFailedJobsRequest**](../Model/SearchFailedJobsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchFailedJobsResponse**](../Model/SearchFailedJobsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

