# Ensi\PimClient\ProductEventsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchProductEvents**](ProductEventsApi.md#searchProductEvents) | **POST** /products/product-events:search | Search for product events



## searchProductEvents

> \Ensi\PimClient\Dto\SearchProductEventsResponse searchProductEvents($search_product_events_request)

Search for product events

Search for product events

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductEventsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_events_request = new \Ensi\PimClient\Dto\SearchProductEventsRequest(); // \Ensi\PimClient\Dto\SearchProductEventsRequest | 

try {
    $result = $apiInstance->searchProductEvents($search_product_events_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductEventsApi->searchProductEvents: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_events_request** | [**\Ensi\PimClient\Dto\SearchProductEventsRequest**](../Model/SearchProductEventsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchProductEventsResponse**](../Model/SearchProductEventsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

