# Ensi\PimClient\ImportsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProductImport**](ImportsApi.md#createProductImport) | **POST** /imports/products | Creating an object of ProductImport and starting import
[**getProductImport**](ImportsApi.md#getProductImport) | **GET** /imports/products/{id} | Get the object of ProductImport by ID
[**preloadImportFile**](ImportsApi.md#preloadImportFile) | **POST** /imports/products:preload-file | Uploading a file for ProductImport
[**searchProductImportWarnings**](ImportsApi.md#searchProductImportWarnings) | **POST** /imports/product-warnings:search | Search for objects of ProductImportWarning
[**searchProductImports**](ImportsApi.md#searchProductImports) | **POST** /imports/products:search | Search for objects of ProductImport



## createProductImport

> \Ensi\PimClient\Dto\ProductImportResponse createProductImport($create_product_import_request)

Creating an object of ProductImport and starting import

Creating an object of ProductImport and starting import

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ImportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_product_import_request = new \Ensi\PimClient\Dto\CreateProductImportRequest(); // \Ensi\PimClient\Dto\CreateProductImportRequest | 

try {
    $result = $apiInstance->createProductImport($create_product_import_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImportsApi->createProductImport: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_product_import_request** | [**\Ensi\PimClient\Dto\CreateProductImportRequest**](../Model/CreateProductImportRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductImportResponse**](../Model/ProductImportResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProductImport

> \Ensi\PimClient\Dto\ProductImportResponse getProductImport($id, $include)

Get the object of ProductImport by ID

Get the object of ProductImport by ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ImportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getProductImport($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImportsApi->getProductImport: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\PimClient\Dto\ProductImportResponse**](../Model/ProductImportResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## preloadImportFile

> \Ensi\PimClient\Dto\PreloadFile preloadImportFile($file)

Uploading a file for ProductImport

Uploading a file for ProductImport

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ImportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->preloadImportFile($file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImportsApi->preloadImportFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\PimClient\Dto\PreloadFile**](../Model/PreloadFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductImportWarnings

> \Ensi\PimClient\Dto\SearchProductImportWarningsResponse searchProductImportWarnings($search_product_import_warnings_request)

Search for objects of ProductImportWarning

Search for objects of ProductImportWarning

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ImportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_import_warnings_request = new \Ensi\PimClient\Dto\SearchProductImportWarningsRequest(); // \Ensi\PimClient\Dto\SearchProductImportWarningsRequest | 

try {
    $result = $apiInstance->searchProductImportWarnings($search_product_import_warnings_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImportsApi->searchProductImportWarnings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_import_warnings_request** | [**\Ensi\PimClient\Dto\SearchProductImportWarningsRequest**](../Model/SearchProductImportWarningsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchProductImportWarningsResponse**](../Model/SearchProductImportWarningsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductImports

> \Ensi\PimClient\Dto\SearchProductImportsResponse searchProductImports($search_product_imports_request)

Search for objects of ProductImport

Search for objects of ProductImport

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ImportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_imports_request = new \Ensi\PimClient\Dto\SearchProductImportsRequest(); // \Ensi\PimClient\Dto\SearchProductImportsRequest | 

try {
    $result = $apiInstance->searchProductImports($search_product_imports_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImportsApi->searchProductImports: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_imports_request** | [**\Ensi\PimClient\Dto\SearchProductImportsRequest**](../Model/SearchProductImportsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchProductImportsResponse**](../Model/SearchProductImportsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

