# Ensi\PimClient\PropertiesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDirectoryValue**](PropertiesApi.md#createDirectoryValue) | **POST** /categories/properties/{id}:add-directory | Запрос на создание нового значения справочника
[**createDirectoryValues**](PropertiesApi.md#createDirectoryValues) | **POST** /categories/properties/{id}:add-directories | Запрос на создание новых значений справочника
[**createProperty**](PropertiesApi.md#createProperty) | **POST** /categories/properties | Запрос на создание нового свойства
[**deleteDirectoryValue**](PropertiesApi.md#deleteDirectoryValue) | **DELETE** /categories/properties/directory/{id} | Запрос на удаление значения справочника
[**deleteProperties**](PropertiesApi.md#deleteProperties) | **POST** /categories/properties:mass-delete | Массовое удаление свойств
[**deleteProperty**](PropertiesApi.md#deleteProperty) | **DELETE** /categories/properties/{id} | Запрос на удаление свойства
[**getDirectoryValue**](PropertiesApi.md#getDirectoryValue) | **GET** /categories/properties/directory/{id} | Возвращает данные значения справочника
[**getProperty**](PropertiesApi.md#getProperty) | **GET** /categories/properties/{id} | Получение данных свойства
[**patchDirectoryValue**](PropertiesApi.md#patchDirectoryValue) | **PATCH** /categories/properties/directory/{id} | Запрос на обновление отдельных атрибутов значения справочника
[**patchProperty**](PropertiesApi.md#patchProperty) | **PATCH** /categories/properties/{id} | Запрос на обновление отдельных атрибутов свойства
[**preloadDirectoryValueFile**](PropertiesApi.md#preloadDirectoryValueFile) | **POST** /categories/properties/directory:preload-file | Загрузка файла для значения справочника
[**preloadDirectoryValueImage**](PropertiesApi.md#preloadDirectoryValueImage) | **POST** /categories/properties/directory:preload-image | Загрузка картинки для значения справочника
[**replaceDirectoryValue**](PropertiesApi.md#replaceDirectoryValue) | **PUT** /categories/properties/directory/{id} | Запрос на обновление значения справочника
[**replaceProperty**](PropertiesApi.md#replaceProperty) | **PUT** /categories/properties/{id} | Запрос на обновление свойства
[**searchDirectoryValues**](PropertiesApi.md#searchDirectoryValues) | **POST** /categories/properties/directory:search | Поиск значений справочников, удовлетворяющих фильтру
[**searchProperties**](PropertiesApi.md#searchProperties) | **POST** /categories/properties:search | Поиск свойств, удовлетворяющих фильтру



## createDirectoryValue

> \Ensi\PimClient\Dto\DirectoryValueResponse createDirectoryValue($id, $create_directory_value_request)

Запрос на создание нового значения справочника

Запрос на создание нового значения справочника

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$create_directory_value_request = new \Ensi\PimClient\Dto\CreateDirectoryValueRequest(); // \Ensi\PimClient\Dto\CreateDirectoryValueRequest | 

try {
    $result = $apiInstance->createDirectoryValue($id, $create_directory_value_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->createDirectoryValue: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **create_directory_value_request** | [**\Ensi\PimClient\Dto\CreateDirectoryValueRequest**](../Model/CreateDirectoryValueRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\DirectoryValueResponse**](../Model/DirectoryValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createDirectoryValues

> \Ensi\PimClient\Dto\MassDirectoryValueResponse createDirectoryValues($id, $create_directory_values_request)

Запрос на создание новых значений справочника

Запрос на создание новых значений справочника

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$create_directory_values_request = new \Ensi\PimClient\Dto\CreateDirectoryValuesRequest(); // \Ensi\PimClient\Dto\CreateDirectoryValuesRequest | 

try {
    $result = $apiInstance->createDirectoryValues($id, $create_directory_values_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->createDirectoryValues: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **create_directory_values_request** | [**\Ensi\PimClient\Dto\CreateDirectoryValuesRequest**](../Model/CreateDirectoryValuesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\MassDirectoryValueResponse**](../Model/MassDirectoryValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createProperty

> \Ensi\PimClient\Dto\PropertyResponse createProperty($create_property_request)

Запрос на создание нового свойства

Запрос на создание нового свойства

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_property_request = new \Ensi\PimClient\Dto\CreatePropertyRequest(); // \Ensi\PimClient\Dto\CreatePropertyRequest | 

try {
    $result = $apiInstance->createProperty($create_property_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->createProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_property_request** | [**\Ensi\PimClient\Dto\CreatePropertyRequest**](../Model/CreatePropertyRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\PropertyResponse**](../Model/PropertyResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDirectoryValue

> \Ensi\PimClient\Dto\EmptyDataResponse deleteDirectoryValue($id)

Запрос на удаление значения справочника

Запрос на удаление значения справочника

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDirectoryValue($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->deleteDirectoryValue: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProperties

> \Ensi\PimClient\Dto\MassOperationResult deleteProperties($request_body_mass_operation)

Массовое удаление свойств

Массовое удаление свойств

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$request_body_mass_operation = new \Ensi\PimClient\Dto\RequestBodyMassOperation(); // \Ensi\PimClient\Dto\RequestBodyMassOperation | 

try {
    $result = $apiInstance->deleteProperties($request_body_mass_operation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->deleteProperties: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body_mass_operation** | [**\Ensi\PimClient\Dto\RequestBodyMassOperation**](../Model/RequestBodyMassOperation.md)|  |

### Return type

[**\Ensi\PimClient\Dto\MassOperationResult**](../Model/MassOperationResult.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProperty

> \Ensi\PimClient\Dto\EmptyDataResponse deleteProperty($id)

Запрос на удаление свойства

Запрос на удаление свойства

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteProperty($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->deleteProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDirectoryValue

> \Ensi\PimClient\Dto\DirectoryValueResponse getDirectoryValue($id)

Возвращает данные значения справочника

Возвращает данные значения справочника

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getDirectoryValue($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->getDirectoryValue: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\DirectoryValueResponse**](../Model/DirectoryValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProperty

> \Ensi\PimClient\Dto\PropertyResponse getProperty($id, $include)

Получение данных свойства

Получение данных свойства

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getProperty($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->getProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\PimClient\Dto\PropertyResponse**](../Model/PropertyResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDirectoryValue

> \Ensi\PimClient\Dto\DirectoryValueResponse patchDirectoryValue($id, $patch_directory_value_request)

Запрос на обновление отдельных атрибутов значения справочника

Запрос на обновление отдельных атрибутов значения справочника

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_directory_value_request = new \Ensi\PimClient\Dto\PatchDirectoryValueRequest(); // \Ensi\PimClient\Dto\PatchDirectoryValueRequest | 

try {
    $result = $apiInstance->patchDirectoryValue($id, $patch_directory_value_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->patchDirectoryValue: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_directory_value_request** | [**\Ensi\PimClient\Dto\PatchDirectoryValueRequest**](../Model/PatchDirectoryValueRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\DirectoryValueResponse**](../Model/DirectoryValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchProperty

> \Ensi\PimClient\Dto\PropertyResponse patchProperty($id, $patch_property_request)

Запрос на обновление отдельных атрибутов свойства

Запрос на обновление отдельных атрибутов свойства

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_property_request = new \Ensi\PimClient\Dto\PatchPropertyRequest(); // \Ensi\PimClient\Dto\PatchPropertyRequest | 

try {
    $result = $apiInstance->patchProperty($id, $patch_property_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->patchProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_property_request** | [**\Ensi\PimClient\Dto\PatchPropertyRequest**](../Model/PatchPropertyRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\PropertyResponse**](../Model/PropertyResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## preloadDirectoryValueFile

> \Ensi\PimClient\Dto\PreloadFile preloadDirectoryValueFile($file)

Загрузка файла для значения справочника

Загрузка файла для значения справочника

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->preloadDirectoryValueFile($file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->preloadDirectoryValueFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\PimClient\Dto\PreloadFile**](../Model/PreloadFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## preloadDirectoryValueImage

> \Ensi\PimClient\Dto\PreloadFile preloadDirectoryValueImage($file)

Загрузка картинки для значения справочника

Загрузка картинки для значения справочника

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->preloadDirectoryValueImage($file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->preloadDirectoryValueImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\PimClient\Dto\PreloadFile**](../Model/PreloadFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceDirectoryValue

> \Ensi\PimClient\Dto\DirectoryValueResponse replaceDirectoryValue($id, $replace_directory_value_request)

Запрос на обновление значения справочника

Запрос на обновление значения справочника

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_directory_value_request = new \Ensi\PimClient\Dto\ReplaceDirectoryValueRequest(); // \Ensi\PimClient\Dto\ReplaceDirectoryValueRequest | 

try {
    $result = $apiInstance->replaceDirectoryValue($id, $replace_directory_value_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->replaceDirectoryValue: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_directory_value_request** | [**\Ensi\PimClient\Dto\ReplaceDirectoryValueRequest**](../Model/ReplaceDirectoryValueRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\DirectoryValueResponse**](../Model/DirectoryValueResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProperty

> \Ensi\PimClient\Dto\PropertyResponse replaceProperty($id, $replace_property_request)

Запрос на обновление свойства

Запрос на обновление свойства

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_property_request = new \Ensi\PimClient\Dto\ReplacePropertyRequest(); // \Ensi\PimClient\Dto\ReplacePropertyRequest | 

try {
    $result = $apiInstance->replaceProperty($id, $replace_property_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->replaceProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_property_request** | [**\Ensi\PimClient\Dto\ReplacePropertyRequest**](../Model/ReplacePropertyRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\PropertyResponse**](../Model/PropertyResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDirectoryValues

> \Ensi\PimClient\Dto\SearchDirectoryValuesResponse searchDirectoryValues($search_directory_values_request)

Поиск значений справочников, удовлетворяющих фильтру

Поиск значений справочников, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_directory_values_request = new \Ensi\PimClient\Dto\SearchDirectoryValuesRequest(); // \Ensi\PimClient\Dto\SearchDirectoryValuesRequest | 

try {
    $result = $apiInstance->searchDirectoryValues($search_directory_values_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->searchDirectoryValues: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_directory_values_request** | [**\Ensi\PimClient\Dto\SearchDirectoryValuesRequest**](../Model/SearchDirectoryValuesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchDirectoryValuesResponse**](../Model/SearchDirectoryValuesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProperties

> \Ensi\PimClient\Dto\SearchPropertiesResponse searchProperties($search_properties_request)

Поиск свойств, удовлетворяющих фильтру

Поиск свойств, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_properties_request = new \Ensi\PimClient\Dto\SearchPropertiesRequest(); // \Ensi\PimClient\Dto\SearchPropertiesRequest | 

try {
    $result = $apiInstance->searchProperties($search_properties_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->searchProperties: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_properties_request** | [**\Ensi\PimClient\Dto\SearchPropertiesRequest**](../Model/SearchPropertiesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchPropertiesResponse**](../Model/SearchPropertiesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

