# Ensi\PimClient\ProductsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProduct**](ProductsApi.md#createProduct) | **POST** /products/products | Creating an object of Product
[**deleteProduct**](ProductsApi.md#deleteProduct) | **DELETE** /products/products/{id} | Deleting an object of Product
[**deleteProductAttributes**](ProductsApi.md#deleteProductAttributes) | **DELETE** /products/products/{id}/attributes | Deleting object of ProductAttribute
[**deleteProductImage**](ProductsApi.md#deleteProductImage) | **POST** /products/products/{id}:delete-image | Deleting product image
[**getProduct**](ProductsApi.md#getProduct) | **GET** /products/published/{id} | Get the object of Product by ID
[**getProductDraft**](ProductsApi.md#getProductDraft) | **GET** /products/products/{id} | Get the object of ProductDraft by ID
[**getProductsCommonAttributes**](ProductsApi.md#getProductsCommonAttributes) | **POST** /products/products:common-attributes | Getting common attributes for selected products
[**massDeleteProducts**](ProductsApi.md#massDeleteProducts) | **POST** /products/products:mass-delete | Mass deleting of object Product
[**massPatchProducts**](ProductsApi.md#massPatchProducts) | **POST** /products/products:mass-patch | Mass patching of object Product
[**massPatchProductsByQuery**](ProductsApi.md#massPatchProductsByQuery) | **POST** /products/products:mass-patch-by-query | Mass patching of object Product by filter
[**patchProduct**](ProductsApi.md#patchProduct) | **PATCH** /products/products/{id} | Patching an object of Product
[**patchProductAttributes**](ProductsApi.md#patchProductAttributes) | **PATCH** /products/products/{id}/attributes | Patching product attributes
[**patchProductImages**](ProductsApi.md#patchProductImages) | **PATCH** /products/products/{id}/images | Patching product images
[**preloadProductImage**](ProductsApi.md#preloadProductImage) | **POST** /products/products:preload-image | Upload image for product or product attribute
[**replaceProduct**](ProductsApi.md#replaceProduct) | **PUT** /products/products/{id} | Replacing an object of Product
[**replaceProductAttributes**](ProductsApi.md#replaceProductAttributes) | **PUT** /products/products/{id}/attributes | Replacing all product attributes
[**replaceProductImages**](ProductsApi.md#replaceProductImages) | **PUT** /products/products/{id}/images | Replacing all product images
[**searchProductDrafts**](ProductsApi.md#searchProductDrafts) | **POST** /products/products:search | Search for objects of ProductDraft
[**searchProducts**](ProductsApi.md#searchProducts) | **POST** /products/published:search | Search for objects of Product
[**uploadProductImage**](ProductsApi.md#uploadProductImage) | **POST** /products/products/{id}:upload-image | Uploading product image



## createProduct

> \Ensi\PimClient\Dto\ProductDraftResponse createProduct($create_product_request)

Creating an object of Product

Creating an object of Product

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_product_request = new \Ensi\PimClient\Dto\CreateProductRequest(); // \Ensi\PimClient\Dto\CreateProductRequest | 

try {
    $result = $apiInstance->createProduct($create_product_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->createProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_product_request** | [**\Ensi\PimClient\Dto\CreateProductRequest**](../Model/CreateProductRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductDraftResponse**](../Model/ProductDraftResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProduct

> \Ensi\PimClient\Dto\EmptyDataResponse deleteProduct($id)

Deleting an object of Product

Deleting an object of Product

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID

try {
    $result = $apiInstance->deleteProduct($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->deleteProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductAttributes

> \Ensi\PimClient\Dto\EmptyDataResponse deleteProductAttributes($id, $delete_product_attributes_request)

Deleting object of ProductAttribute

Deleting object of ProductAttribute

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$delete_product_attributes_request = new \Ensi\PimClient\Dto\DeleteProductAttributesRequest(); // \Ensi\PimClient\Dto\DeleteProductAttributesRequest | 

try {
    $result = $apiInstance->deleteProductAttributes($id, $delete_product_attributes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->deleteProductAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **delete_product_attributes_request** | [**\Ensi\PimClient\Dto\DeleteProductAttributesRequest**](../Model/DeleteProductAttributesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductImage

> \Ensi\PimClient\Dto\EmptyDataResponse deleteProductImage($id, $product_image_delete_request)

Deleting product image

Deleting product image

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$product_image_delete_request = new \Ensi\PimClient\Dto\ProductImageDeleteRequest(); // \Ensi\PimClient\Dto\ProductImageDeleteRequest | 

try {
    $result = $apiInstance->deleteProductImage($id, $product_image_delete_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->deleteProductImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **product_image_delete_request** | [**\Ensi\PimClient\Dto\ProductImageDeleteRequest**](../Model/ProductImageDeleteRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProduct

> \Ensi\PimClient\Dto\ProductResponse getProduct($id, $include)

Get the object of Product by ID

Get the object of Product by ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getProduct($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\PimClient\Dto\ProductResponse**](../Model/ProductResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProductDraft

> \Ensi\PimClient\Dto\ProductDraftResponse getProductDraft($id, $include)

Get the object of ProductDraft by ID

Get the object of ProductDraft by ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getProductDraft($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getProductDraft: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\PimClient\Dto\ProductDraftResponse**](../Model/ProductDraftResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProductsCommonAttributes

> \Ensi\PimClient\Dto\ProductsCommonAttributesResponse getProductsCommonAttributes($products_common_attributes_request)

Getting common attributes for selected products

Getting common attributes for selected products

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$products_common_attributes_request = new \Ensi\PimClient\Dto\ProductsCommonAttributesRequest(); // \Ensi\PimClient\Dto\ProductsCommonAttributesRequest | 

try {
    $result = $apiInstance->getProductsCommonAttributes($products_common_attributes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getProductsCommonAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **products_common_attributes_request** | [**\Ensi\PimClient\Dto\ProductsCommonAttributesRequest**](../Model/ProductsCommonAttributesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductsCommonAttributesResponse**](../Model/ProductsCommonAttributesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## massDeleteProducts

> \Ensi\PimClient\Dto\EmptyDataResponse massDeleteProducts($request_body_mass_operation)

Mass deleting of object Product

Mass deleting of object Product

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$request_body_mass_operation = new \Ensi\PimClient\Dto\RequestBodyMassOperation(); // \Ensi\PimClient\Dto\RequestBodyMassOperation | 

try {
    $result = $apiInstance->massDeleteProducts($request_body_mass_operation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->massDeleteProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body_mass_operation** | [**\Ensi\PimClient\Dto\RequestBodyMassOperation**](../Model/RequestBodyMassOperation.md)|  |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## massPatchProducts

> \Ensi\PimClient\Dto\MassOperationResult massPatchProducts($mass_patch_products_request)

Mass patching of object Product

Mass patching of object Product

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mass_patch_products_request = new \Ensi\PimClient\Dto\MassPatchProductsRequest(); // \Ensi\PimClient\Dto\MassPatchProductsRequest | 

try {
    $result = $apiInstance->massPatchProducts($mass_patch_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->massPatchProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mass_patch_products_request** | [**\Ensi\PimClient\Dto\MassPatchProductsRequest**](../Model/MassPatchProductsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\MassOperationResult**](../Model/MassOperationResult.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## massPatchProductsByQuery

> \Ensi\PimClient\Dto\EmptyDataResponse massPatchProductsByQuery($mass_patch_products_by_query_request)

Mass patching of object Product by filter

Mass patching of object Product by filter

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mass_patch_products_by_query_request = new \Ensi\PimClient\Dto\MassPatchProductsByQueryRequest(); // \Ensi\PimClient\Dto\MassPatchProductsByQueryRequest | 

try {
    $result = $apiInstance->massPatchProductsByQuery($mass_patch_products_by_query_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->massPatchProductsByQuery: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mass_patch_products_by_query_request** | [**\Ensi\PimClient\Dto\MassPatchProductsByQueryRequest**](../Model/MassPatchProductsByQueryRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchProduct

> \Ensi\PimClient\Dto\ProductDraftResponse patchProduct($id, $patch_product_request)

Patching an object of Product

Patching an object of Product

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$patch_product_request = new \Ensi\PimClient\Dto\PatchProductRequest(); // \Ensi\PimClient\Dto\PatchProductRequest | 

try {
    $result = $apiInstance->patchProduct($id, $patch_product_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->patchProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **patch_product_request** | [**\Ensi\PimClient\Dto\PatchProductRequest**](../Model/PatchProductRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductDraftResponse**](../Model/ProductDraftResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchProductAttributes

> \Ensi\PimClient\Dto\ProductAttributesResponse patchProductAttributes($id, $patch_product_attributes_request)

Patching product attributes

Creating new attributes and updating specified ones. Attributes not specified in the request won't be changed

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$patch_product_attributes_request = new \Ensi\PimClient\Dto\PatchProductAttributesRequest(); // \Ensi\PimClient\Dto\PatchProductAttributesRequest | 

try {
    $result = $apiInstance->patchProductAttributes($id, $patch_product_attributes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->patchProductAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **patch_product_attributes_request** | [**\Ensi\PimClient\Dto\PatchProductAttributesRequest**](../Model/PatchProductAttributesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductAttributesResponse**](../Model/ProductAttributesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchProductImages

> \Ensi\PimClient\Dto\ProductImagesResponse patchProductImages($id, $patch_product_images_request)

Patching product images

Creating new images and updating specified ones. Images not specified in the request won't be changed

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$patch_product_images_request = new \Ensi\PimClient\Dto\PatchProductImagesRequest(); // \Ensi\PimClient\Dto\PatchProductImagesRequest | 

try {
    $result = $apiInstance->patchProductImages($id, $patch_product_images_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->patchProductImages: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **patch_product_images_request** | [**\Ensi\PimClient\Dto\PatchProductImagesRequest**](../Model/PatchProductImagesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductImagesResponse**](../Model/ProductImagesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## preloadProductImage

> \Ensi\PimClient\Dto\PreloadFile preloadProductImage($file)

Upload image for product or product attribute

Upload image for product or product attribute

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->preloadProductImage($file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->preloadProductImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\PimClient\Dto\PreloadFile**](../Model/PreloadFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProduct

> \Ensi\PimClient\Dto\ProductDraftResponse replaceProduct($id, $replace_product_request)

Replacing an object of Product

Replacing an object of Product

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$replace_product_request = new \Ensi\PimClient\Dto\ReplaceProductRequest(); // \Ensi\PimClient\Dto\ReplaceProductRequest | 

try {
    $result = $apiInstance->replaceProduct($id, $replace_product_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->replaceProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **replace_product_request** | [**\Ensi\PimClient\Dto\ReplaceProductRequest**](../Model/ReplaceProductRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductDraftResponse**](../Model/ProductDraftResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProductAttributes

> \Ensi\PimClient\Dto\ProductAttributesResponse replaceProductAttributes($id, $replace_product_attributes_request)

Replacing all product attributes

Replacing all product attributes. All not specified in the request attributes will be deleted

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$replace_product_attributes_request = new \Ensi\PimClient\Dto\ReplaceProductAttributesRequest(); // \Ensi\PimClient\Dto\ReplaceProductAttributesRequest | 

try {
    $result = $apiInstance->replaceProductAttributes($id, $replace_product_attributes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->replaceProductAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **replace_product_attributes_request** | [**\Ensi\PimClient\Dto\ReplaceProductAttributesRequest**](../Model/ReplaceProductAttributesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductAttributesResponse**](../Model/ProductAttributesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProductImages

> \Ensi\PimClient\Dto\ProductImagesResponse replaceProductImages($id, $replace_product_images_request)

Replacing all product images

Replacing all product images. All not specified in request images will be deleted

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$replace_product_images_request = new \Ensi\PimClient\Dto\ReplaceProductImagesRequest(); // \Ensi\PimClient\Dto\ReplaceProductImagesRequest | 

try {
    $result = $apiInstance->replaceProductImages($id, $replace_product_images_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->replaceProductImages: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **replace_product_images_request** | [**\Ensi\PimClient\Dto\ReplaceProductImagesRequest**](../Model/ReplaceProductImagesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductImagesResponse**](../Model/ProductImagesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductDrafts

> \Ensi\PimClient\Dto\SearchProductDraftsResponse searchProductDrafts($search_product_drafts_request)

Search for objects of ProductDraft

Search for objects of ProductDraft

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_drafts_request = new \Ensi\PimClient\Dto\SearchProductDraftsRequest(); // \Ensi\PimClient\Dto\SearchProductDraftsRequest | 

try {
    $result = $apiInstance->searchProductDrafts($search_product_drafts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->searchProductDrafts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_drafts_request** | [**\Ensi\PimClient\Dto\SearchProductDraftsRequest**](../Model/SearchProductDraftsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchProductDraftsResponse**](../Model/SearchProductDraftsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProducts

> \Ensi\PimClient\Dto\SearchProductsResponse searchProducts($search_products_request)

Search for objects of Product

Search for objects of Product

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_products_request = new \Ensi\PimClient\Dto\SearchProductsRequest(); // \Ensi\PimClient\Dto\SearchProductsRequest | 

try {
    $result = $apiInstance->searchProducts($search_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->searchProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_products_request** | [**\Ensi\PimClient\Dto\SearchProductsRequest**](../Model/SearchProductsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchProductsResponse**](../Model/SearchProductsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## uploadProductImage

> \Ensi\PimClient\Dto\ProductImageResponse uploadProductImage($id, $product_image_upload_request)

Uploading product image

Uploading product image

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$product_image_upload_request = new \Ensi\PimClient\Dto\ProductImageUploadRequest(); // \Ensi\PimClient\Dto\ProductImageUploadRequest | 

try {
    $result = $apiInstance->uploadProductImage($id, $product_image_upload_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->uploadProductImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **product_image_upload_request** | [**\Ensi\PimClient\Dto\ProductImageUploadRequest**](../Model/ProductImageUploadRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductImageResponse**](../Model/ProductImageResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

