# Ensi\PimClient\ProductFieldsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProductField**](ProductFieldsApi.md#getProductField) | **GET** /classifiers/product-fields/{id} | Получение настроек поля по идентификатору
[**patchProductField**](ProductFieldsApi.md#patchProductField) | **PATCH** /classifiers/product-fields/{id} | Обновление настроек поля товара
[**searchProductFields**](ProductFieldsApi.md#searchProductFields) | **POST** /classifiers/product-fields:search | Поиск полей товаров, удовлетворяющих условиям отбора



## getProductField

> \Ensi\PimClient\Dto\ProductFieldResponse getProductField($id)

Получение настроек поля по идентификатору

Получение настроек поля по идентификатору

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductFieldsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getProductField($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductFieldsApi->getProductField: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\ProductFieldResponse**](../Model/ProductFieldResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchProductField

> \Ensi\PimClient\Dto\ProductFieldResponse patchProductField($id, $patch_product_field_request)

Обновление настроек поля товара

Обновление настроек поля товара

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductFieldsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_product_field_request = new \Ensi\PimClient\Dto\PatchProductFieldRequest(); // \Ensi\PimClient\Dto\PatchProductFieldRequest | 

try {
    $result = $apiInstance->patchProductField($id, $patch_product_field_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductFieldsApi->patchProductField: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_product_field_request** | [**\Ensi\PimClient\Dto\PatchProductFieldRequest**](../Model/PatchProductFieldRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductFieldResponse**](../Model/ProductFieldResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductFields

> \Ensi\PimClient\Dto\SearchProductFieldsResponse searchProductFields($search_product_fields_request)

Поиск полей товаров, удовлетворяющих условиям отбора

Поиск полей товаров, удовлетворяющих условиям отбора

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductFieldsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_fields_request = new \Ensi\PimClient\Dto\SearchProductFieldsRequest(); // \Ensi\PimClient\Dto\SearchProductFieldsRequest | 

try {
    $result = $apiInstance->searchProductFields($search_product_fields_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductFieldsApi->searchProductFields: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_fields_request** | [**\Ensi\PimClient\Dto\SearchProductFieldsRequest**](../Model/SearchProductFieldsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchProductFieldsResponse**](../Model/SearchProductFieldsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

