<?php
/**
 * SearchCategoriesFilterTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\PimClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi PIM.
 *
 * PIM
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\PimClient;

use PHPUnit\Framework\TestCase;

/**
 * SearchCategoriesFilterTest Class Doc Comment
 *
 * @category    Class
 * @description SearchCategoriesFilter
 * @package     Ensi\PimClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class SearchCategoriesFilterTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "SearchCategoriesFilter"
     */
    public function testSearchCategoriesFilter()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "name_like"
     */
    public function testPropertyNameLike()
    {
    }

    /**
     * Test attribute "code"
     */
    public function testPropertyCode()
    {
    }

    /**
     * Test attribute "code_like"
     */
    public function testPropertyCodeLike()
    {
    }

    /**
     * Test attribute "parent_id"
     */
    public function testPropertyParentId()
    {
    }

    /**
     * Test attribute "is_active"
     */
    public function testPropertyIsActive()
    {
    }

    /**
     * Test attribute "is_real_active"
     */
    public function testPropertyIsRealActive()
    {
    }

    /**
     * Test attribute "is_root"
     */
    public function testPropertyIsRoot()
    {
    }

    /**
     * Test attribute "exclude_id"
     */
    public function testPropertyExcludeId()
    {
    }

    /**
     * Test attribute "has_is_gluing"
     */
    public function testPropertyHasIsGluing()
    {
    }
}
