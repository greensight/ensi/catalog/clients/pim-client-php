<?php
/**
 * MassPatchProductsRequestTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\PimClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi PIM.
 *
 * PIM
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\PimClient;

use PHPUnit\Framework\TestCase;

/**
 * MassPatchProductsRequestTest Class Doc Comment
 *
 * @category    Class
 * @description MassPatchProductsRequest
 * @package     Ensi\PimClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class MassPatchProductsRequestTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "MassPatchProductsRequest"
     */
    public function testMassPatchProductsRequest()
    {
    }

    /**
     * Test attribute "ids"
     */
    public function testPropertyIds()
    {
    }

    /**
     * Test attribute "fields"
     */
    public function testPropertyFields()
    {
    }

    /**
     * Test attribute "attributes"
     */
    public function testPropertyAttributes()
    {
    }
}
