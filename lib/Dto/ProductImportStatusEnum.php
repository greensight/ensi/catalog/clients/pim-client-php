<?php
/**
 * ProductImportStatusEnum
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\PimClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi PIM.
 *
 * PIM
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\PimClient\Dto;
use \Ensi\PimClient\ObjectSerializer;

/**
 * ProductImportStatusEnum Class Doc Comment
 *
 * @category Class
 * @description Тип импорта:  * 1 - Создан, ожидает обработки  * 2 - Обрабатывается  * 3 - Успешно завершён  * 4 - Завершён с ошибкой
 * @package  Ensi\PimClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class ProductImportStatusEnum
{
    
    /** Создан, ожидает обработки */
    public const NEW = 1;
    
    /** Обрабатывается */
    public const IN_PROCESS = 2;
    
    /** Успешно завершён */
    public const DONE = 3;
    
    /** Завершён с ошибкой */
    public const FAILED = 4;
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::NEW,
            self::IN_PROCESS,
            self::DONE,
            self::FAILED,
        ];
    }

    /**
    * Gets allowable values and titles of the enum
    * @return string[]
    */
    public static function getDescriptions(): array
    {
        return [
            self::NEW => 'Создан, ожидает обработки',
            self::IN_PROCESS => 'Обрабатывается',
            self::DONE => 'Успешно завершён',
            self::FAILED => 'Завершён с ошибкой',
        ];
    }
}


