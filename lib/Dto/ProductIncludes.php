<?php
/**
 * ProductIncludes
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\PimClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi PIM.
 *
 * PIM
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\PimClient\Dto;

use \ArrayAccess;
use \Ensi\PimClient\ObjectSerializer;

/**
 * ProductIncludes Class Doc Comment
 *
 * @category Class
 * @package  Ensi\PimClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class ProductIncludes implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'ProductIncludes';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'brand' => '\Ensi\PimClient\Dto\Brand',
        'categories' => '\Ensi\PimClient\Dto\Category[]',
        'category_product_links' => '\Ensi\PimClient\Dto\CategoryProductLink[]',
        'images' => '\Ensi\PimClient\Dto\ProductImage[]',
        'attributes' => '\Ensi\PimClient\Dto\ProductAttributeValue[]',
        'product_groups' => '\Ensi\PimClient\Dto\ProductGroup[]',
        'no_filled_required_attributes' => 'bool'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPIFormats = [
        'brand' => null,
        'categories' => null,
        'category_product_links' => null,
        'images' => null,
        'attributes' => null,
        'product_groups' => null,
        'no_filled_required_attributes' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static $openAPINullables = [
        'brand' => false,
        'categories' => false,
        'category_product_links' => false,
        'images' => false,
        'attributes' => false,
        'product_groups' => false,
        'no_filled_required_attributes' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

        /**
     * Array of property to nullable mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPINullables()
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return array
     */
    public function getOpenAPINullablesSetToNull()
    {
        return $this->openAPINullablesSetToNull;
    }

    public function setOpenAPINullablesSetToNull($nullablesSetToNull)
    {
        $this->openAPINullablesSetToNull=$nullablesSetToNull;
        return $this;
    }

    /**
     * Checks if a property is nullable
     *
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        if (isset(self::openAPINullables()[$property])) {
            return self::openAPINullables()[$property];
        }

        return false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        if (in_array($property, $this->getOpenAPINullablesSetToNull())) {
            return true;
        }
        return false;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'brand' => 'brand',
        'categories' => 'categories',
        'category_product_links' => 'category_product_links',
        'images' => 'images',
        'attributes' => 'attributes',
        'product_groups' => 'product_groups',
        'no_filled_required_attributes' => 'no_filled_required_attributes'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'brand' => 'setBrand',
        'categories' => 'setCategories',
        'category_product_links' => 'setCategoryProductLinks',
        'images' => 'setImages',
        'attributes' => 'setAttributes',
        'product_groups' => 'setProductGroups',
        'no_filled_required_attributes' => 'setNoFilledRequiredAttributes'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'brand' => 'getBrand',
        'categories' => 'getCategories',
        'category_product_links' => 'getCategoryProductLinks',
        'images' => 'getImages',
        'attributes' => 'getAttributes',
        'product_groups' => 'getProductGroups',
        'no_filled_required_attributes' => 'getNoFilledRequiredAttributes'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('brand', $data, null);
        $this->setIfExists('categories', $data, null);
        $this->setIfExists('category_product_links', $data, null);
        $this->setIfExists('images', $data, null);
        $this->setIfExists('attributes', $data, null);
        $this->setIfExists('product_groups', $data, null);
        $this->setIfExists('no_filled_required_attributes', $data, null);
    }

    public function setIfExists(string $variableName, $fields, $defaultValue)
    {
        if (is_array($fields) && array_key_exists($variableName, $fields) && is_null($fields[$variableName]) && self::isNullable($variableName)) {
            array_push($this->openAPINullablesSetToNull, $variableName);
        }

        $this->container[$variableName] = isset($fields[$variableName]) ? $fields[$variableName] : $defaultValue;

        return $this;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets brand
     *
     * @return \Ensi\PimClient\Dto\Brand|null
     */
    public function getBrand()
    {
        return $this->container['brand'];
    }

    /**
     * Sets brand
     *
     * @param \Ensi\PimClient\Dto\Brand|null $brand brand
     *
     * @return $this
     */
    public function setBrand($brand)
    {


        /*if (is_null($brand)) {
            throw new \InvalidArgumentException('non-nullable brand cannot be null');
        }*/
        $this->container['brand'] = $brand;

        return $this;
    }

    /**
     * Gets categories
     *
     * @return \Ensi\PimClient\Dto\Category[]|null
     */
    public function getCategories()
    {
        return $this->container['categories'];
    }

    /**
     * Sets categories
     *
     * @param \Ensi\PimClient\Dto\Category[]|null $categories categories
     *
     * @return $this
     */
    public function setCategories($categories)
    {


        /*if (is_null($categories)) {
            throw new \InvalidArgumentException('non-nullable categories cannot be null');
        }*/
        $this->container['categories'] = $categories;

        return $this;
    }

    /**
     * Gets category_product_links
     *
     * @return \Ensi\PimClient\Dto\CategoryProductLink[]|null
     */
    public function getCategoryProductLinks()
    {
        return $this->container['category_product_links'];
    }

    /**
     * Sets category_product_links
     *
     * @param \Ensi\PimClient\Dto\CategoryProductLink[]|null $category_product_links category_product_links
     *
     * @return $this
     */
    public function setCategoryProductLinks($category_product_links)
    {


        /*if (is_null($category_product_links)) {
            throw new \InvalidArgumentException('non-nullable category_product_links cannot be null');
        }*/
        $this->container['category_product_links'] = $category_product_links;

        return $this;
    }

    /**
     * Gets images
     *
     * @return \Ensi\PimClient\Dto\ProductImage[]|null
     */
    public function getImages()
    {
        return $this->container['images'];
    }

    /**
     * Sets images
     *
     * @param \Ensi\PimClient\Dto\ProductImage[]|null $images images
     *
     * @return $this
     */
    public function setImages($images)
    {


        /*if (is_null($images)) {
            throw new \InvalidArgumentException('non-nullable images cannot be null');
        }*/
        $this->container['images'] = $images;

        return $this;
    }

    /**
     * Gets attributes
     *
     * @return \Ensi\PimClient\Dto\ProductAttributeValue[]|null
     */
    public function getAttributes()
    {
        return $this->container['attributes'];
    }

    /**
     * Sets attributes
     *
     * @param \Ensi\PimClient\Dto\ProductAttributeValue[]|null $attributes attributes
     *
     * @return $this
     */
    public function setAttributes($attributes)
    {


        /*if (is_null($attributes)) {
            throw new \InvalidArgumentException('non-nullable attributes cannot be null');
        }*/
        $this->container['attributes'] = $attributes;

        return $this;
    }

    /**
     * Gets product_groups
     *
     * @return \Ensi\PimClient\Dto\ProductGroup[]|null
     */
    public function getProductGroups()
    {
        return $this->container['product_groups'];
    }

    /**
     * Sets product_groups
     *
     * @param \Ensi\PimClient\Dto\ProductGroup[]|null $product_groups product_groups
     *
     * @return $this
     */
    public function setProductGroups($product_groups)
    {


        /*if (is_null($product_groups)) {
            throw new \InvalidArgumentException('non-nullable product_groups cannot be null');
        }*/
        $this->container['product_groups'] = $product_groups;

        return $this;
    }

    /**
     * Gets no_filled_required_attributes
     *
     * @return bool|null
     */
    public function getNoFilledRequiredAttributes()
    {
        return $this->container['no_filled_required_attributes'];
    }

    /**
     * Sets no_filled_required_attributes
     *
     * @param bool|null $no_filled_required_attributes Product has not filled required attributes
     *
     * @return $this
     */
    public function setNoFilledRequiredAttributes($no_filled_required_attributes)
    {


        /*if (is_null($no_filled_required_attributes)) {
            throw new \InvalidArgumentException('non-nullable no_filled_required_attributes cannot be null');
        }*/
        $this->container['no_filled_required_attributes'] = $no_filled_required_attributes;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset): mixed
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }

    /**
    * Convert object to array
    *
    * @return array
    */
    public function toArray()
    {
        return json_decode(json_encode(ObjectSerializer::sanitizeForSerialization($this)), true);
    }
}


