<?php
/**
 * MetricsCategoryEnum
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\PimClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi PIM.
 *
 * PIM
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\PimClient\Dto;
use \Ensi\PimClient\ObjectSerializer;

/**
 * MetricsCategoryEnum Class Doc Comment
 *
 * @category Class
 * @description Тип данных:  * &#x60;master&#x60; - Мастер-данные  * &#x60;content&#x60; - Контент и медиа  * &#x60;attributes&#x60; - Атрибуты
 * @package  Ensi\PimClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class MetricsCategoryEnum
{
    
    /** Мастер-данные */
    public const MASTER = 'master';
    
    /** Контент и медиа */
    public const CONTENT = 'content';
    
    /** Атрибуты */
    public const ATTRIBUTES = 'attributes';
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::MASTER,
            self::CONTENT,
            self::ATTRIBUTES,
        ];
    }

    /**
    * Gets allowable values and titles of the enum
    * @return string[]
    */
    public static function getDescriptions(): array
    {
        return [
            self::MASTER => 'Мастер-данные',
            self::CONTENT => 'Контент и медиа',
            self::ATTRIBUTES => 'Атрибуты',
        ];
    }
}


