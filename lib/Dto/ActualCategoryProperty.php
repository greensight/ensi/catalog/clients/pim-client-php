<?php
/**
 * ActualCategoryProperty
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\PimClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi PIM.
 *
 * PIM
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\PimClient\Dto;

use \ArrayAccess;
use \Ensi\PimClient\ObjectSerializer;

/**
 * ActualCategoryProperty Class Doc Comment
 *
 * @category Class
 * @package  Ensi\PimClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class ActualCategoryProperty implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'ActualCategoryProperty';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'id' => 'int',
        'category_id' => 'int',
        'property_id' => 'int',
        'created_at' => '\DateTime',
        'updated_at' => '\DateTime',
        'is_required' => 'bool',
        'is_gluing' => 'bool',
        'is_inherited' => 'bool',
        'is_common' => 'bool',
        'category' => '\Ensi\PimClient\Dto\Category',
        'property' => '\Ensi\PimClient\Dto\Property'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPIFormats = [
        'id' => null,
        'category_id' => null,
        'property_id' => null,
        'created_at' => 'date-time',
        'updated_at' => 'date-time',
        'is_required' => null,
        'is_gluing' => null,
        'is_inherited' => null,
        'is_common' => null,
        'category' => null,
        'property' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static $openAPINullables = [
        'id' => false,
        'category_id' => false,
        'property_id' => false,
        'created_at' => false,
        'updated_at' => false,
        'is_required' => false,
        'is_gluing' => false,
        'is_inherited' => false,
        'is_common' => false,
        'category' => false,
        'property' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

        /**
     * Array of property to nullable mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPINullables()
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return array
     */
    public function getOpenAPINullablesSetToNull()
    {
        return $this->openAPINullablesSetToNull;
    }

    public function setOpenAPINullablesSetToNull($nullablesSetToNull)
    {
        $this->openAPINullablesSetToNull=$nullablesSetToNull;
        return $this;
    }

    /**
     * Checks if a property is nullable
     *
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        if (isset(self::openAPINullables()[$property])) {
            return self::openAPINullables()[$property];
        }

        return false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        if (in_array($property, $this->getOpenAPINullablesSetToNull())) {
            return true;
        }
        return false;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'category_id' => 'category_id',
        'property_id' => 'property_id',
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'is_required' => 'is_required',
        'is_gluing' => 'is_gluing',
        'is_inherited' => 'is_inherited',
        'is_common' => 'is_common',
        'category' => 'category',
        'property' => 'property'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'category_id' => 'setCategoryId',
        'property_id' => 'setPropertyId',
        'created_at' => 'setCreatedAt',
        'updated_at' => 'setUpdatedAt',
        'is_required' => 'setIsRequired',
        'is_gluing' => 'setIsGluing',
        'is_inherited' => 'setIsInherited',
        'is_common' => 'setIsCommon',
        'category' => 'setCategory',
        'property' => 'setProperty'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'category_id' => 'getCategoryId',
        'property_id' => 'getPropertyId',
        'created_at' => 'getCreatedAt',
        'updated_at' => 'getUpdatedAt',
        'is_required' => 'getIsRequired',
        'is_gluing' => 'getIsGluing',
        'is_inherited' => 'getIsInherited',
        'is_common' => 'getIsCommon',
        'category' => 'getCategory',
        'property' => 'getProperty'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('id', $data, null);
        $this->setIfExists('category_id', $data, null);
        $this->setIfExists('property_id', $data, null);
        $this->setIfExists('created_at', $data, null);
        $this->setIfExists('updated_at', $data, null);
        $this->setIfExists('is_required', $data, null);
        $this->setIfExists('is_gluing', $data, null);
        $this->setIfExists('is_inherited', $data, null);
        $this->setIfExists('is_common', $data, null);
        $this->setIfExists('category', $data, null);
        $this->setIfExists('property', $data, null);
    }

    public function setIfExists(string $variableName, $fields, $defaultValue)
    {
        if (is_array($fields) && array_key_exists($variableName, $fields) && is_null($fields[$variableName]) && self::isNullable($variableName)) {
            array_push($this->openAPINullablesSetToNull, $variableName);
        }

        $this->container[$variableName] = isset($fields[$variableName]) ? $fields[$variableName] : $defaultValue;

        return $this;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['id'] === null) {
            $invalidProperties[] = "'id' can't be null";
        }
        if ($this->container['category_id'] === null) {
            $invalidProperties[] = "'category_id' can't be null";
        }
        if ($this->container['property_id'] === null) {
            $invalidProperties[] = "'property_id' can't be null";
        }
        if ($this->container['created_at'] === null) {
            $invalidProperties[] = "'created_at' can't be null";
        }
        if ($this->container['updated_at'] === null) {
            $invalidProperties[] = "'updated_at' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id id
     *
     * @return $this
     */
    public function setId($id)
    {


        /*if (is_null($id)) {
            throw new \InvalidArgumentException('non-nullable id cannot be null');
        }*/
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets category_id
     *
     * @return int
     */
    public function getCategoryId()
    {
        return $this->container['category_id'];
    }

    /**
     * Sets category_id
     *
     * @param int $category_id Category ID
     *
     * @return $this
     */
    public function setCategoryId($category_id)
    {


        /*if (is_null($category_id)) {
            throw new \InvalidArgumentException('non-nullable category_id cannot be null');
        }*/
        $this->container['category_id'] = $category_id;

        return $this;
    }

    /**
     * Gets property_id
     *
     * @return int
     */
    public function getPropertyId()
    {
        return $this->container['property_id'];
    }

    /**
     * Sets property_id
     *
     * @param int $property_id Property ID
     *
     * @return $this
     */
    public function setPropertyId($property_id)
    {


        /*if (is_null($property_id)) {
            throw new \InvalidArgumentException('non-nullable property_id cannot be null');
        }*/
        $this->container['property_id'] = $property_id;

        return $this;
    }

    /**
     * Gets created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->container['created_at'];
    }

    /**
     * Sets created_at
     *
     * @param \DateTime $created_at Date of creation
     *
     * @return $this
     */
    public function setCreatedAt($created_at)
    {


        /*if (is_null($created_at)) {
            throw new \InvalidArgumentException('non-nullable created_at cannot be null');
        }*/
        $this->container['created_at'] = $created_at;

        return $this;
    }

    /**
     * Gets updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->container['updated_at'];
    }

    /**
     * Sets updated_at
     *
     * @param \DateTime $updated_at Date of update
     *
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {


        /*if (is_null($updated_at)) {
            throw new \InvalidArgumentException('non-nullable updated_at cannot be null');
        }*/
        $this->container['updated_at'] = $updated_at;

        return $this;
    }

    /**
     * Gets is_required
     *
     * @return bool|null
     */
    public function getIsRequired()
    {
        return $this->container['is_required'];
    }

    /**
     * Sets is_required
     *
     * @param bool|null $is_required The attribute must be filled in
     *
     * @return $this
     */
    public function setIsRequired($is_required)
    {


        /*if (is_null($is_required)) {
            throw new \InvalidArgumentException('non-nullable is_required cannot be null');
        }*/
        $this->container['is_required'] = $is_required;

        return $this;
    }

    /**
     * Gets is_gluing
     *
     * @return bool|null
     */
    public function getIsGluing()
    {
        return $this->container['is_gluing'];
    }

    /**
     * Sets is_gluing
     *
     * @param bool|null $is_gluing The property is used for gluing products
     *
     * @return $this
     */
    public function setIsGluing($is_gluing)
    {


        /*if (is_null($is_gluing)) {
            throw new \InvalidArgumentException('non-nullable is_gluing cannot be null');
        }*/
        $this->container['is_gluing'] = $is_gluing;

        return $this;
    }

    /**
     * Gets is_inherited
     *
     * @return bool|null
     */
    public function getIsInherited()
    {
        return $this->container['is_inherited'];
    }

    /**
     * Sets is_inherited
     *
     * @param bool|null $is_inherited The property is inherited from parent category
     *
     * @return $this
     */
    public function setIsInherited($is_inherited)
    {


        /*if (is_null($is_inherited)) {
            throw new \InvalidArgumentException('non-nullable is_inherited cannot be null');
        }*/
        $this->container['is_inherited'] = $is_inherited;

        return $this;
    }

    /**
     * Gets is_common
     *
     * @return bool|null
     */
    public function getIsCommon()
    {
        return $this->container['is_common'];
    }

    /**
     * Sets is_common
     *
     * @param bool|null $is_common The property is common to all categories
     *
     * @return $this
     */
    public function setIsCommon($is_common)
    {


        /*if (is_null($is_common)) {
            throw new \InvalidArgumentException('non-nullable is_common cannot be null');
        }*/
        $this->container['is_common'] = $is_common;

        return $this;
    }

    /**
     * Gets category
     *
     * @return \Ensi\PimClient\Dto\Category|null
     */
    public function getCategory()
    {
        return $this->container['category'];
    }

    /**
     * Sets category
     *
     * @param \Ensi\PimClient\Dto\Category|null $category category
     *
     * @return $this
     */
    public function setCategory($category)
    {


        /*if (is_null($category)) {
            throw new \InvalidArgumentException('non-nullable category cannot be null');
        }*/
        $this->container['category'] = $category;

        return $this;
    }

    /**
     * Gets property
     *
     * @return \Ensi\PimClient\Dto\Property|null
     */
    public function getProperty()
    {
        return $this->container['property'];
    }

    /**
     * Sets property
     *
     * @param \Ensi\PimClient\Dto\Property|null $property property
     *
     * @return $this
     */
    public function setProperty($property)
    {


        /*if (is_null($property)) {
            throw new \InvalidArgumentException('non-nullable property cannot be null');
        }*/
        $this->container['property'] = $property;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset): mixed
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }

    /**
    * Convert object to array
    *
    * @return array
    */
    public function toArray()
    {
        return json_decode(json_encode(ObjectSerializer::sanitizeForSerialization($this)), true);
    }
}


